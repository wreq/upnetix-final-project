# Social Media Application

An application for people who want to upload and share their pictures and moments from their amazing life. :)

## Development server

You must first install the dependancies with the command `npm install`, be sure to run the command in the `/client` directory of the project.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Login/Register

In order to browse and use most of the functioanlities in the application you must login (or register if needed).
Before logging in you can only see posts which status is public(more info on post status bellow).

## The Beggining

The beggining of our app is the home page, here you are able to see some posts which are public and you do not have to follow the posts user to see it. 

## Posts 

When you login/register to the application you are able to see the posts feed which consist of the posts of users you follow.
After you follow a user you are able to see the private posts which the user shared. You are able to like each post and leave a thoughtful comment.

Every logged user has the abillity to upload a post of his own consisting of a picture and a description. Every post has a public or private status which hides the private posts from users who do not follow you.

## Profile

In the profile page you can see the collection of the posts you uploaded and edit their description if needed. There is also the ability to delete a post which you deem unworthy to be a part of your awesome profile page.

You can also edit the information about you here, like your profile picture, bio or personal profile information.

Each user can follow some other user which gives them the ability to see their private posts and follow what they share in the home page.

## Search

The search bar has the power of finding a user by simply puttin their username in the field.

### Enjoy!