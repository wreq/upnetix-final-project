import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormBuilder } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreatePostComponent } from './create-post.component';
import { Routes, Router } from '@angular/router';
import { AuthGuard } from '../../core/guards/auth.guard';
import { PostDetailComponent } from '../post-detail/post-detail.component';
import { AllPostsComponent } from '../all-posts/all-posts.component';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostsRoutingModule } from '../posts-routing.module';
import { AllPostsResolverService } from '../resolvers/all-posts-resolver.service';
import { PostResolverService } from '../resolvers/post-resolver.service';
import { PostService } from '../services/post.service';
import { TokenInterceptorService } from '../../core/services/imgur/token-interceptor.service';
import { AuthService } from '../../core/services/auth.service';
import { of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

describe('CreatePostComponent', () => {
  let component: CreatePostComponent;
  let fixture: ComponentFixture<CreatePostComponent>;

  const routes: Routes = [
    { path: '', redirectTo: 'all', pathMatch: 'full' },
    { path: 'create', component: CreatePostComponent },
    { path: '**', redirectTo: '/not-found' }
  ];

  const authService = {
    get loggedUserData$() { return of(); },
    login() { },
    register() { },
  };

  const notificator = {
    success() { },
    error() { },
  };

  const postService = {
    createPost() { return of(); }
  };

  let router: Router;

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        CommonModule,
        FontAwesomeModule,
        PostsRoutingModule,
      ],
      declarations: [
        PostDetailComponent,
        CreatePostComponent,
        AllPostsComponent,
      ],
      providers: [
        AllPostsResolverService,
        PostResolverService,
        PostService,
        TokenInterceptorService,
        FormBuilder
      ]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ToastrService, { useValue: notificator })
      .overrideProvider(PostService, { useValue: postService })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    expect(component).toBeDefined();
  });

  it('should get logged user id on initialisation', () => {

    const spy = jest.spyOn(authService, 'loggedUserData$', 'get').mockImplementation(() => of({id: '1'}));

    component.ngOnInit();

    expect(component.loggedUserId).toEqual('1');

  });
});
