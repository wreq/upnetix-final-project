import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PostDTO } from '../models/post.dto';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from '../services/post.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostStatus } from '../../config/post-status.enum';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../users/models/user.dto';
import { tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  createPostForm: FormGroup;
  post: PostDTO;
  caption: string;
  postStatus: PostStatus;
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  createdPostUrl;
  loggedUserId: string;

  constructor(
    private notify: ToastrService,
    private postsService: PostService,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.auth.loggedUserData$.subscribe((data) => {
      this.loggedUserId = data.id;
    }, () => { });

    this.createPostForm = this.formBuilder.group({
      caption: ['', [Validators.required]],
      coverUrl: ['', [Validators.required]],
      postStatus: ['1', [Validators.required]]
    }, () => { });

  }



  fileProgress(fileInput: any) {
    this.fileData = fileInput.target.files[0];
    this.preview();
  }

  imageDrop(files: any) {
    this.fileData = files[0];
    this.preview();
  }


  preview() {
    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      // should validate in the form controler
      this.notify.error('Only files of type image allowed');
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    // tslint:disable-next-line: variable-name
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  get getCaption() { return this.createPostForm.get('caption'); }

  get getCoverUrl() { return this.createPostForm.get('coverUrl'); }

  public submitPost() {
    const newPost = this.createPostForm.value;
    this.createPostForm.value.postStatus ?
      newPost.postStatus = PostStatus.Public.toString() :
      newPost.postStatus = PostStatus.Private.toString();
    this.postsService.postImgAndGetUrl(this.fileData).pipe(tap(
      (data) => {
        newPost.coverUrl = data.data.link;
      }),
      switchMap(() => this.postsService.createPost(newPost))).subscribe((data) => {
        this.notify.success(`Post created!`);
        this.post = data;
        this.router.navigate(['/users', this.loggedUserId]);
      },
        (err) => {
          this.createPostForm.reset();
          this.notify.error('Creation of post failed!');
        });
  }
}
