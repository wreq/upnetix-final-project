import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PostDTO } from '../models/post.dto';
import { PostService } from '../services/post.service';
import { AuthService } from '../../core/services/auth.service';
import { switchMap, take } from 'rxjs/operators';
import { CONFIG } from '../../config/config';
import { UsersService } from '../../core/services/users.service';

@Injectable({
  providedIn: 'root'
})
export class AllPostsResolverService implements Resolve<PostDTO[]> {

  constructor(
    private postsService: PostService,
    private authService: AuthService,
    private usersService: UsersService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {

    return this.authService.loggedUserData$.pipe(
      switchMap((loggedUser) => loggedUser ?
      this.postsService.getFollowingPosts(0, CONFIG.ALL_POST_PAGE_POSTS_TAKE) :
      this.postsService.getAllPosts(0, CONFIG.ALL_POST_PAGE_POSTS_TAKE)),
      take(1)
    );
  }
}

