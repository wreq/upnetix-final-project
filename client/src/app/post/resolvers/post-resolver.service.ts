import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { PostDTO } from '../models/post.dto';
import { PostService } from '../services/post.service';

@Injectable({
  providedIn: 'root'
})
export class PostResolverService implements Resolve<PostDTO> {

  constructor(
    private postService: PostService,
    private router: Router,
    private notify: ToastrService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<PostDTO> {
    const postId: string = route.paramMap.get('id');
    return this.postService.getIndividualPost(postId).pipe(
      map(post => {
        if (post) {
          return post;
        } else {
          this.router.navigate(['/not-found']);
          this.notify.error(`No post with id ${postId}!`);
          return;
        }
      }),
    );
  }
}

