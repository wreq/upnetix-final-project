import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostDTO } from '../models/post.dto';
import { ToastrService } from 'ngx-toastr';
import { PostService } from '../services/post.service';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../users/models/user.dto';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  @Input() post: PostDTO;

  @Output() postCoverClick = new EventEmitter();

  isPostLiked = false;
  loggedUser: UserDTO;

  constructor(
    private authService: AuthService,
    private notify: ToastrService,
    private postsService: PostService,
  ) { }

  ngOnInit() {
    this.authService.loggedUserData$.subscribe((loggedUser) => {
      if (loggedUser && loggedUser.liked) {
        this.isPostLiked = loggedUser.liked.some((x) => x.id === this.post.id);
      }

      this.loggedUser = loggedUser;
    }, () => { });
  }

  likePost() {
    this.postsService.likePost(this.post.id).subscribe(({ user, post }) => {
      if (this.isPostLiked) {
        this.notify.success('Post disliked');
      } else {
        this.notify.success('Post liked');
      }
      this.post = post;
      this.authService.emitUserData(user);
    }, () => { });
  }

  postClickHandle(ev) {
    this.postCoverClick.emit(this.post);
  }
}
