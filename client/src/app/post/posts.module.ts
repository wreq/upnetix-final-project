import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { SharedModule } from '../shared/shared.module';
import { PostService } from './services/post.service';
import { PostsRoutingModule } from './posts-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostResolverService } from './resolvers/post-resolver.service';
import { TokenInterceptorService } from '../core/services/imgur/token-interceptor.service';
import { CreatePostComponent } from './create-post/create-post.component';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { AllPostsResolverService } from './resolvers/all-posts-resolver.service';



@NgModule({
  declarations: [
    PostDetailComponent,
    CreatePostComponent,
    AllPostsComponent,
    ],
  imports: [
    SharedModule,
    CommonModule,
    FontAwesomeModule,
    PostsRoutingModule,
  ],
  providers: [
    AllPostsResolverService,
    PostResolverService,
    PostService,
    TokenInterceptorService,
  ],
  exports: [
    PostDetailComponent,
    CreatePostComponent,
    AllPostsComponent
  ]
})
export class PostsModule { }
