import { async, TestBed } from '@angular/core/testing';
import { PostService } from './post.service';
import { PostDetailComponent } from '../post-detail/post-detail.component';
import { CreatePostComponent } from '../create-post/create-post.component';
import { AllPostsComponent } from '../all-posts/all-posts.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';



describe('PostService', () => {

  let service: PostService;

  let http;

  beforeEach( async( () => {
    // clear all spies and mocks
    jest.clearAllMocks();

    http = {
      get() { },
      put() { },
      post() { },
      delete() { },
      patch() { },
    };

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],

      providers: [
        PostService,
      ]
    })
      .overrideProvider(HttpClient, { useValue: http });

    service = TestBed.get(PostService);
  }));

  it('should create an instance', () => {
    expect(service).toBeDefined();
  });

  it('getAllPosts should', () => {

    const spy = jest.spyOn(http, 'get');

    service.getAllPosts(1, 2);

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/public?take=2&skip=1`);

  });

  it('getFollowingPosts should', () => {

    const spy = jest.spyOn(http, 'get');

    service.getFollowingPosts(1, 2);

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/feed?take=2&skip=1`);

  });

  it('getIndividualPosts should ', () => {

    const spy = jest.spyOn(http, 'get');

    service.getIndividualPost('1');

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/1`);

  });

  it('deletePosts should ', () => {

    const spy = jest.spyOn(http, 'delete');

    service.deletePost('1');

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/1`);

  });

  it('updatePosts should ', () => {

    const spy = jest.spyOn(http, 'put');

    service.updatePost('1', {
      id: '1',
      caption: 'test',
      coverUrl: 'testUrl',
      isDeleted: false,
    });

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/1`, {
      id: '1',
      caption: 'test',
      coverUrl: 'testUrl',
      isDeleted: false,
    });

  });

  it('createPost should ', () => {

    const spy = jest.spyOn(http, 'post');

    service.createPost({
      id: '1',
      caption: 'test',
      coverUrl: 'testUrl',
      isDeleted: false,
    });

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/`, {
      id: '1',
      caption: 'test',
      coverUrl: 'testUrl',
      isDeleted: false,
    });

  });

  it('getUserPosts should ', () => {

    const spy = jest.spyOn(http, 'get');

    service.getUserPosts('1', 2, 3);

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/users/1/posts?skip=2&take=3`);

  });

  it('postImgAndGetUrl should ', () => {

    const spy = jest.spyOn(http, 'post');

    service.postImgAndGetUrl('1');

    expect(spy).toHaveBeenCalledWith(`https://api.imgur.com/3/image`, '1');

  });

  it('getRandomImg should ', () => {

    const spy = jest.spyOn(http, 'get');

    service.getRandomImg();

    expect(spy).toHaveBeenCalledWith(`https://api.imgur.com/3/gallery/random/random`);

  });

  it('getPostComments should ', () => {

    const spy = jest.spyOn(http, 'get');

    service.getPostComments('1');

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/1/comments`);

  });

  it('likePost should ', () => {

    const spy = jest.spyOn(http, 'patch');

    service.likePost('1');

    expect(spy).toHaveBeenCalledWith(`http://localhost:3000/posts/1`, {action: 'likePost'});

  });

});

