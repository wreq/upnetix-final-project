
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../../config/config';
import { PostDTO } from '../models/post.dto';
import { UpdatePostDTO } from '../models/update-post.dto';
import { PostStatus } from '../../config/post-status.enum';
import { CommentDTO } from '../../core/models/comment.dto';
import { UserDTO } from '../../users/models/user.dto';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private baseUrl = CONFIG.API_DOMAIN_NAME;
  public constructor(private readonly http: HttpClient) { }

  getAllPosts(skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${this.baseUrl}/posts/public?take=${take}&skip=${skip}`);
  }

  getFollowingPosts(skip: number, take: number): Observable<PostDTO[]> {
    return this.http.get<PostDTO[]>(`${this.baseUrl}/posts/feed?take=${take}&skip=${skip}`);
  }

  getIndividualPost(postId: string): Observable<PostDTO> {
    return this.http.get<PostDTO>(`${this.baseUrl}/posts/${postId}`);
  }

  deletePost(postId: string): Observable<object> {
    return this.http.delete(`${this.baseUrl}/posts/${postId}`);
  }

  updatePost(postId: string, body: Partial<UpdatePostDTO>): Observable<PostDTO> {
    return this.http.put<PostDTO>(`${this.baseUrl}/posts/${postId}`, body);
  }

  createPost(body: UpdatePostDTO): Observable<PostDTO> {
    return this.http.post<PostDTO>(`${this.baseUrl}/posts/`, body);
  }

  getUserPosts(userId: string, skip: number, take: number, postStatus?: PostStatus): Observable<PostDTO[]> {
    const query = postStatus ? `&postStatus=${postStatus}` : '';
    return this.http.get<PostDTO[]>(`${this.baseUrl}/users/${userId}/posts?skip=${skip}&take=${take}${query}`);
  }

  postImgAndGetUrl(file): Observable<any> {
    return this.http.post(`https://api.imgur.com/3/image`, file);
  }

  getRandomImg() {
    return this.http.get(`https://api.imgur.com/3/gallery/random/random`);
  }

  getPostComments(postId: string): Observable<CommentDTO[]> {
    return this.http.get<CommentDTO[]>(`${this.baseUrl}/posts/${postId}/comments`);
  }

  likePost(postId: string): Observable<{ user: UserDTO, post: PostDTO }> {
    return this.http.patch<{ user: UserDTO, post: PostDTO }>(`${this.baseUrl}/posts/${postId}`, { action: 'likePost'});
  }
}
