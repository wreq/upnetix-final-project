import { PostStatus } from '../../config/post-status.enum';
import { UserDTO } from '../../users/models/user.dto';

export class PostDTO {
  id: string;
  caption: string;
  postStatus: PostStatus;
  coverUrl: string;
  user: UserDTO;
  likesCount: number;
  commentsCount: number;
  isDeleted: boolean;
  createdAt: string;
}
