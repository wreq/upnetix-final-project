import { PostStatus } from '../../config/post-status.enum';

export class UpdatePostDTO {
    id: string;
    caption: string;
    coverUrl: string;
    postStatus: PostStatus;
    isDeleted: boolean;
}
