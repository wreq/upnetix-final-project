import { UserDTO } from '../../users/models/user.dto';

export class CommentDTO {
  id: string;
  user: UserDTO;
  caption: string;
}
