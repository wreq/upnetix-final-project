import { Component, OnInit } from '@angular/core';
import { PostDTO } from '../models/post.dto';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostModalComponent } from '../../shared/post-modal/post-modal.component';
import { PostService } from '../services/post.service';
import { delay, switchMap, tap, take } from 'rxjs/operators';
import { CONFIG } from '../../config/config';
import { UsersService } from '../../core/services/users.service';
import { UserDTO } from '../../users/models/user.dto';
import { of } from 'rxjs';
import { PostStatus } from '../../config/post-status.enum';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.css']
})
export class AllPostsComponent implements OnInit {

  posts: PostDTO[] = [];
  isUserAdmin: boolean;
  loading = false;
  loggedUser: UserDTO;
  users: UserDTO[];
  take = CONFIG.ALL_POST_PAGE_POSTS_TAKE;
  skip = 0;

  constructor(
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private postsService: PostService,
    private usersService: UsersService,
  ) { }

  ngOnInit() {
    this.route.data.pipe(
      tap(({ loggedUser, posts }) => ( this.posts = posts, this.loggedUser = loggedUser )),
      switchMap(({ loggedUser, posts }) => posts.length ? (this.skip = posts.length, of([])) : this.usersService.getAllUsers()),

      take(1)
    ).subscribe((users) => {
      this.users = users.filter((x) => x.id !== this.loggedUser.id);
    }, () => {});
  }

  open(post: PostDTO) {
    const modalRef = this.modalService.open(PostModalComponent, { windowClass: 'post-modal' });
    modalRef.componentInstance.post = post;
  }

  onScroll() {
    this.loading = true;
    this.choosePosts(this.loggedUser)
    .subscribe((data) => {
      this.posts = [...this.posts, ...data];
      this.skip += this.take;
      this.loading = false;
    },
    () => {
      this.loading = false;
    }, () => {});
  }

  private choosePosts(user) {
    if (user) {
      return this.postsService.getFollowingPosts(this.skip, this.take);
    } else {
      return this.postsService.getAllPosts(this.skip, this.take);
    }
  }
}
