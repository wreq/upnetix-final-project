import { NgModule, Optional, SkipSelf, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostResolverService } from './resolvers/post-resolver.service';
import { CreatePostComponent } from './create-post/create-post.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { AllPostsResolverService } from './resolvers/all-posts-resolver.service';
import { LoggedUserResolverService } from '../config/logged-user.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'all', pathMatch: 'full' },
  { path: 'all', component: AllPostsComponent, resolve: { loggedUser: LoggedUserResolverService, posts: AllPostsResolverService } },
  { path: 'create', component: CreatePostComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule {
  public constructor(@Optional() @SkipSelf() parent: PostsRoutingModule) {
    if (parent) {
      return parent;
    }
  }
}


