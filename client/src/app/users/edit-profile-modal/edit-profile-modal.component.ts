import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDTO } from '../models/user.dto';
import { UsersService } from '../../core/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/storage.service';
import { MustMatch } from '../services/must-match.service';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit-profile-modal',
  templateUrl: './edit-profile-modal.component.html',
  styleUrls: ['./edit-profile-modal.component.css']
})
export class EditProfileModalComponent implements OnInit {

  editProfileForm: FormGroup;
  @Input() user: UserDTO;

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private authService: AuthService,
    private storage: StorageService,
    private notify: ToastrService,
    private activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {

    this.editProfileForm = this.formBuilder.group({
      username: [
        this.user.username,
        Validators.compose([Validators.minLength(2), Validators.maxLength(20)])
      ],
      email: [
        this.user.email,
        Validators.compose([Validators.email])
      ],
      password: ['',
        Validators.compose([,
        Validators.minLength(6),
        Validators.maxLength(20)])
      ],
      confirmPassword: [
        '', Validators.compose([])
      ],
      publicInfo: [this.user.publicInfo,
        Validators.compose([
        Validators.minLength(6),
        Validators.maxLength(200)])],
    },
    {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  get form() { return this.editProfileForm.controls as any; }

  get username() { return this.editProfileForm.get('username'); }

  get email() { return this.editProfileForm.get('email'); }

  get password() { return this.editProfileForm.get('password'); }

  editUserProfile() {
    if (this.editProfileForm.value.password === '') {
      delete this.editProfileForm.value.password;
    }

    this.usersService.updateLoggedUser(this.editProfileForm.value)
      .subscribe(({ user, token }) => {
        this.user = user;
        this.authService.emitUserData(user);
        this.storage.setItem('token', token);
        this.notify.success('Profile updated');
        this.activeModal.close();
      }, () => { });
  }

}
