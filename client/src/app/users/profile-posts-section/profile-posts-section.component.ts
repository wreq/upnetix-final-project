import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostDTO } from '../../post/models/post.dto';
import { UserDTO } from '../models/user.dto';
import { UsersRelation } from '../../config/users-relation.enum';
import { PostStatus } from '../../config/post-status.enum';

@Component({
  selector: 'app-profile-posts-section',
  templateUrl: './profile-posts-section.component.html',
  styleUrls: ['./profile-posts-section.component.css']
})
export class ProfilePostsSectionComponent implements OnInit {

  @Input() posts: PostDTO[];
  @Input() usersRelation: UsersRelation;
  @Input() userId: string;
  allView = true;
  tab = '1';
  initialized = false;
  isDropdownActive = false;

  constructor() { }

  get UsersRelation() { return UsersRelation; }

  get PostStatus() { return PostStatus; }

  ngOnInit() {
    this.initialized = true;
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnChanges() {
    if (this.initialized) {
      if (this.allView && this.usersRelation === UsersRelation.NotFollowing) {
        this.allView = false;
      }

      if (this.usersRelation === UsersRelation.Following) {
        this.allView = false;
        this.tab = '2';
      }

      if (this.usersRelation === UsersRelation.NotFollowing) {
        this.allView = true;
        this.tab = '1';
      }
    }
  }

}
