import { Pipe, PipeTransform } from '@angular/core';
import { PostDTO } from '../../post/models/post.dto';
import { PostStatus } from '../../config/post-status.enum';

@Pipe({ name: 'postStatusFiterPipe' })
export class PostStatusFiterPipe implements PipeTransform {
  transform(posts: PostDTO[], status: PostStatus) {
    if (!status) {
      return posts;
    }

    return posts.filter(post => post.postStatus === status);
  }
}
