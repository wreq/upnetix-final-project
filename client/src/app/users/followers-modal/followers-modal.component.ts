import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserDTO } from '../models/user.dto';
import { UsersRelation } from '../../config/users-relation.enum';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-followers-modal',
  templateUrl: './followers-modal.component.html',
  styleUrls: ['./followers-modal.component.css']
})
export class FollowersModalComponent implements OnInit {

  @Input() users: UserDTO[];
  @Input() targetUser: UserDTO;
  @Input() listHeader: string;
  loggedUser: UserDTO;

  constructor(
    public activeModal: NgbActiveModal,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.authService.loggedUserData$.subscribe((data) => {
      this.loggedUser = data;
    }, () => { });
  }

  get UsersRelation() { return UsersRelation; }

  unfollowUserHandle(user: UserDTO) {
    if (this.listHeader === 'Following' && this.targetUser.id === this.loggedUser.id) {
      this.users.splice(this.users.indexOf(this.users.find((x) => x.id === user.id)), 1);
    }
  }

}
