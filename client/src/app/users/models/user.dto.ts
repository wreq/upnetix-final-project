import { PostDTO } from '../../post/models/post.dto';

export class UserDTO {
  id: string;
  username: string;
  email: string;
  publicInfo: string;
  profilePicUrl: string;
  followers: UserDTO[];
  following: UserDTO[];
  followersCount: number;
  followingCount: number;
  postsCount: number;
  liked: PostDTO[];
  roles: string[];
  isDeleted: boolean;
}
