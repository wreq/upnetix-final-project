import { Routes, Router } from '@angular/router';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from '../profile/profile.component';
import { ProfileSectionComponent } from '../profile-section/profile-section.component';
import { ProfilePostsSectionComponent } from '../profile-posts-section/profile-posts-section.component';
import { EditProfileModalComponent } from '../edit-profile-modal/edit-profile-modal.component';
import { FollowersModalComponent } from '../followers-modal/followers-modal.component';
import { UsersRoutingModule } from '../users-routing.module';
import { CommonModule } from '@angular/common';
import { UsersService } from '../../core/services/users.service';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';

describe('RegisterComponent', () => {

    const routes: Routes = [
        { path: '', redirectTo: 'home', pathMatch: 'full' },
        { path: 'users/login', component: LoginComponent },
    ];

    const authService = {
        login() { },
        register() { },
    };
    const notificator = {
        success() { },
        error() { },
    };


    let router: Router;
    let fixture: ComponentFixture<RegisterComponent>;
    let component: RegisterComponent;

    beforeEach(async(() => {
        // clear all spies and mocks
        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
                UsersRoutingModule,
                CommonModule
            ],
            declarations: [LoginComponent,
                RegisterComponent,
                ProfileComponent,
                ProfileSectionComponent,
                ProfilePostsSectionComponent,
                EditProfileModalComponent,
                FollowersModalComponent],
            providers: [
                UsersService,
                FormBuilder,
            ]
        })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(ToastrService, { useValue: notificator })
            .compileComponents();

        router = TestBed.get(Router);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create an instance', () => {
        expect(component).toBeDefined();
    });

    it('register should call AuthService.register with the user data', () => {

        const registerSpy = jest.spyOn(authService, 'register').mockImplementation(() => of(true));

        component.registerForm.controls.username.setValue('TestUser');
        component.registerForm.controls.password.setValue('testpassword');
        component.registerForm.controls.email.setValue('admin@admin.com');
        component.registerForm.controls.confirmPassword.setValue('testpassword');

        component.register(component.registerForm.value);

        fixture.detectChanges();

        expect(registerSpy).toHaveBeenCalledWith({
            username: 'TestUser',
            email: 'admin@admin.com',
            password: 'testpassword',
            confirmPassword: 'testpassword'
        });

    });

    it('successful register should notify', () => {

        const successSpy = jest.spyOn(notificator, 'success');
        const registerSpy = jest.spyOn(authService, 'register').mockImplementation(() => of(true));

        component.registerForm.controls.username.setValue('TestUser');
        component.registerForm.controls.password.setValue('testpassword');
        component.registerForm.controls.email.setValue('admin@admin.com');
        component.registerForm.controls.confirmPassword.setValue('testpassword');

        component.register(component.registerForm.value);

        fixture.detectChanges();

        expect(successSpy).toHaveBeenCalledWith(`Successful register!`);

    });

    it('successful register should navigate to users/login', () => {

        const navigateSpy = jest.spyOn(router, 'navigate');
        const registerSpy = jest.spyOn(authService, 'register').mockImplementation(() => of(true));
        const loginSpy = jest.spyOn(authService, 'login').mockImplementation(() => of(true));

        component.registerForm.controls.username.setValue('TestUser');
        component.registerForm.controls.password.setValue('testpassword');
        component.registerForm.controls.email.setValue('admin@admin.com');
        component.registerForm.controls.confirmPassword.setValue('testpassword');

        component.register(component.registerForm.value);

        fixture.detectChanges();

        expect(router.navigate).toHaveBeenCalledWith(['/users/login']);

    });

    it('unsuccessful register should notify', () => {

        const errorSpy = jest.spyOn(notificator, 'error');
        const registerSpy = jest.spyOn(authService, 'register').mockImplementation(() => throwError(new Error()));

        component.registerForm.controls.username.setValue('TestUser');
        component.registerForm.controls.password.setValue('testpassword');
        component.registerForm.controls.email.setValue('admin@admin.com');
        component.registerForm.controls.confirmPassword.setValue('testpassword');

        component.register(component.registerForm.value);

        fixture.detectChanges();

        expect(notificator.error).toHaveBeenCalledWith(`Registration failed!`);

    });

});
