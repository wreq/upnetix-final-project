import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserCredentialsDTO } from '../models/user-credentials.dto';
import { MustMatch } from '../services/must-match.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../core/services/auth.service';
import { catchError, switchMap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public constructor(
    private readonly authService: AuthService,
    private readonly notify: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {
    this.registerForm = this.formBuilder.group({
      username: [
        '',
        Validators.compose(
          [Validators.required, Validators.minLength(2), Validators.maxLength(20)])
      ],
      password: [
        '', Validators.compose(
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20)
            // Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$/)
          ])
      ],
      email: [
        '', Validators.compose(
          [
            Validators.email,
            Validators.required,
          ])
      ],
      confirmPassword: [
        '', Validators.compose(
          [
            Validators.required,
          ])
      ],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }



  public ngOnInit() {

  }

  public register(user: UserCredentialsDTO) {
    this.authService.register(user).pipe(
      catchError((err) => throwError(err)),
      switchMap(() => this.authService.login({ usernameOrEmail: user.username, ...user }))
    ).subscribe(
      () => {
        this.notify.success('Successful register!');
        this.router.navigate(['/posts/all']);
      },
      (err) => {
        if (err.error.message === 'Username taken') {
          this.notify.error('Username taken');
        } else if (err.error.message === 'Email taken') {
          this.notify.error('Email taken');
        } else {
          this.notify.error('Registration failed!');
        }
      }
    );
  }

  get form() { return this.registerForm.controls as any; }

  get username() { return this.registerForm.get('username'); }

  get email() { return this.registerForm.get('email'); }

  get password() { return this.registerForm.get('password'); }
}
