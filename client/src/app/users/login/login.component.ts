import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserCredentialsDTO } from '../models/user-credentials.dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notification: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameOrEmail: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  login(user: UserCredentialsDTO) {
    this.authService.login(user).subscribe(
      () => {
        this.notification.success(`Successful login!`);
        this.router.navigate(['/posts/all']);
      },
      (err) => {
        this.loginForm.reset();
        this.notification.error('Login failed!');
      }
    );
  }
}
