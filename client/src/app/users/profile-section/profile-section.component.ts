import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserDTO } from '../models/user.dto';
import { UsersRelation } from '../../config/users-relation.enum';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditProfileModalComponent } from '../edit-profile-modal/edit-profile-modal.component';
import { FollowersModalComponent } from '../followers-modal/followers-modal.component';
import { PostService } from '../../post/services/post.service';
import { UsersService } from '../../core/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { tap, switchMap } from 'rxjs/operators';
import { StorageService } from '../../core/services/storage.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile-section',
  templateUrl: './profile-section.component.html',
  styleUrls: ['./profile-section.component.css']
})
export class ProfileSectionComponent implements OnInit {

  @Input() user: UserDTO;
  @Input() usersRelation: UsersRelation;
  @Output() followUser = new EventEmitter();
  @Output() unfollowUser = new EventEmitter();
  loggedUser: UserDTO;
  constructor(
    private modalService: NgbModal,
    private postService: PostService,
    private userService: UsersService,
    private authService: AuthService,
    private storage: StorageService,
    private notify: ToastrService
  ) { }

  ngOnInit() {
    this.authService.loggedUserData$.pipe(
      tap((loggedUser) => {
        if (loggedUser.id === this.user.id) {
          this.user = loggedUser;
        }
      })
    ).subscribe((data) => this.loggedUser = data, () => { });
  }

  get UsersRelation() { return UsersRelation; }

  editProfilePic(fileInput: any) {
    const fileData = fileInput.target.files[0];
    this.postService.postImgAndGetUrl(fileData).pipe(tap(
      (data) => {
        this.loggedUser.profilePicUrl = data.data.link;
      }
    ),
      switchMap(() =>
        this.userService.updateLoggedUser(this.loggedUser)
      )).subscribe(({ user, token }) => {
        this.user = user;
        this.authService.emitUserData(user);
        this.storage.setItem('token', token);
        this.notify.success('Profile picture updated');
      },
        (err) => {
          this.notify.error('Uploading of new profile picture failed!');
        });
  }

  editProfile() {
    const modalRef = this.modalService.open(EditProfileModalComponent, { windowClass: 'profile-edit-modal' });
    modalRef.componentInstance.user = this.user;
  }

  showFollowers() {
    const modalRef = this.modalService.open(FollowersModalComponent, { windowClass: 'followers-modal' });
    modalRef.componentInstance.users = this.user.followers;
    modalRef.componentInstance.targetUser = this.user;
    modalRef.componentInstance.listHeader = 'Followers';
  }

  showFollowing() {
    const modalRef = this.modalService.open(FollowersModalComponent, { windowClass: 'followers-modal' });
    modalRef.componentInstance.users = this.user.following;
    modalRef.componentInstance.targetUser = this.user;
    modalRef.componentInstance.listHeader = 'Following';
  }
}
