import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of, EMPTY, Subject, combineLatest } from 'rxjs';
import { PostService } from '../../post/services/post.service';
import { PostDTO } from '../../post/models/post.dto';
import { PostStatus } from '../../config/post-status.enum';
import { AuthService } from '../../core/services/auth.service';
import { switchMap, take, map, tap } from 'rxjs/operators';
import { UsersService } from '../../core/services/users.service';
import { UserDTO } from '../models/user.dto';
import { UsersRelation } from '../../config/users-relation.enum';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class ProfileResolver implements Resolve<any> {

  constructor(
    private postsService: PostService,
    private authService: AuthService,
    private usersService: UsersService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const userId: string = route.paramMap.get('id');
    let data: { loggedUser: UserDTO, user: UserDTO, usersRelation: UsersRelation };
    return this.authService.loggedUserData$
      .pipe(
        switchMap((loggedUser) =>
          combineLatest(this.usersService.getUser(loggedUser.id), this.usersService.getUser(userId))),
        tap(([loggedUser, user]) => {
          data = { loggedUser, user, usersRelation: this.authService.checkRelation(user, loggedUser) };
          this.authService.emitUserData(loggedUser);
        }),
        take(1),
        switchMap((_) => {
          if (data.usersRelation === UsersRelation.NotFollowing) {
            return this.postsService
              .getUserPosts(userId, 0, CONFIG.PROFILE_PAGE_POSTS_TAKE, PostStatus.Public);
          }

          return this.postsService
            .getUserPosts(userId, 0, CONFIG.PROFILE_PAGE_POSTS_TAKE);
        }),
        map((posts) => ({...data, posts})),
      );
  }
}

