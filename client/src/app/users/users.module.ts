import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from '../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersService } from '../core/services/users.service';
import { ProfileSectionComponent } from './profile-section/profile-section.component';
import { ProfilePostsSectionComponent } from './profile-posts-section/profile-posts-section.component';
import { EditProfileModalComponent } from './edit-profile-modal/edit-profile-modal.component';
import { FollowersModalComponent } from './followers-modal/followers-modal.component';



@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ProfileSectionComponent,
    ProfilePostsSectionComponent,
    EditProfileModalComponent,
    FollowersModalComponent
  ],
  imports: [SharedModule,
     UsersRoutingModule,
     CommonModule],
  providers: [
    UsersService
  ],
  entryComponents: [
    EditProfileModalComponent,
    FollowersModalComponent,
  ]
})
export class UsersModule { }
