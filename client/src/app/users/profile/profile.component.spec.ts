import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { BrowserModule } from '@angular/platform-browser';
import { RegisterComponent } from '../register/register.component';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from './profile.component';
import { AuthGuard } from '../../core/guards/auth.guard';
import { SharedModule } from '../../shared/shared.module';
import { ProfileSectionComponent } from '../profile-section/profile-section.component';
import { ProfilePostsSectionComponent } from '../profile-posts-section/profile-posts-section.component';
import { EditProfileModalComponent } from '../edit-profile-modal/edit-profile-modal.component';
import { FollowersModalComponent } from '../followers-modal/followers-modal.component';
import { UsersService } from '../../core/services/users.service';
import { CoreModule } from '../../core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from '../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { UsersRelation } from '../../config/users-relation.enum';
import { ProfileResolver } from '../resolvers/profile-resolver';

describe('ProfileComponent', () => {

  const activatedRoute = {
    // default initialize with empty array of heroes
    data: of({
      data: {
        user: {},
        posts: [],
        usersRelation: 2
      }
    }),
  };

  const authService = {
    get loggedUserData$() {
      return of();
    },
    emitUserData(user) {

    },
    checkRelation(user, loggedUser) {

    }
  };

  const usersService = {
    followUser() {
      return of();
    },
    unfollowUser() {
      return of();
    }
  };

  const notify = {
    success(msg) {

    }
  };

  let router: Router;
  let fixture: ComponentFixture<ProfileComponent>;
  let component: ProfileComponent;

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        BrowserAnimationsModule,
        CoreModule,
        BrowserModule,
        HttpClientModule,
        NoopAnimationsModule,
        JwtModule.forRoot({ config: {} })
      ],
      declarations: [
        LoginComponent,
        RegisterComponent,
        ProfileComponent,
        ProfileSectionComponent,
        ProfilePostsSectionComponent,
        EditProfileModalComponent,
        FollowersModalComponent
      ],
      providers: [
        UsersService,
      ]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(ToastrService, { useValue: notify })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  it('should be defined', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;

    expect(component).toBeDefined();
  });

  it('should initialize correctly with the data passed from the resolver', () => {

    const user = { id: '1' };
    const posts = [];
    const usersRelation = 1;

    // change the activated route's data per test
    activatedRoute.data = of({ data: { user, posts, usersRelation } });

    // create the component and run all changes on init
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.user).toBe(user);
    expect(component.posts).toBe(posts);
    expect(component.usersRelation).toEqual(usersRelation);
  });

  it('expect target user to be logged user if they have the same id', () => {

    const user = { id: '1' };
    const loggedUser = { id: '1' };
    const posts = [];
    const usersRelation = 2;


    activatedRoute.data = of({ data: { user, posts, usersRelation } });

    jest.spyOn(authService, 'loggedUserData$', 'get')
      .mockImplementation(() => of(loggedUser));

    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.user).toBe(loggedUser);
  });

  it('expect authService.checkcheckRelation to be called with correct arguments', () => {

    const user = { id: '1' };
    const loggedUser = { id: '1' };
    const posts = [];
    const usersRelation = 2;


    activatedRoute.data = of({ data: { user, posts, usersRelation } });

    jest.spyOn(authService, 'loggedUserData$', 'get')
      .mockImplementation(() => of(loggedUser));

    const spy = jest.spyOn(authService, 'checkRelation')
      .mockImplementation(() => 42);

    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(user, loggedUser);
    expect(component.usersRelation).toEqual(42);
  });

  it('expect component.loggedUser to get subject data', () => {

    const user = { id: '1' };
    const loggedUser = { id: '1' };
    const posts = [];
    const usersRelation = 2;


    activatedRoute.data = of({ data: { user, posts, usersRelation } });

    jest.spyOn(authService, 'loggedUserData$', 'get')
      .mockImplementation(() => of(loggedUser));

    const spy = jest.spyOn(authService, 'checkRelation')
      .mockImplementation(() => 42);

    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.loggedUser).toEqual(loggedUser);
  });

  describe('followUser method', () => {
    it('expect usersService.followUser to have been called with correct args', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const loggedUser = { id: '1' };
      const updatedLoggedUser = { id: '3' };
      const posts = [];
      const usersRelation = 2;

      activatedRoute.data = of({ data: { user, posts, usersRelation } });

      const spy = jest.spyOn(usersService, 'followUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      fixture = TestBed.createComponent(ProfileComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.followUser();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(user.id);
    });

    it('expect userRelationChange to return correct function', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const loggedUser = { id: '1' };
      const updatedLoggedUser = { id: '3' };
      const posts = [];
      const usersRelation = 2;

      activatedRoute.data = of({ data: { user, posts, usersRelation } });

      jest.spyOn(usersService, 'followUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      const spy = jest.spyOn(authService, 'emitUserData');

      const notifySpy = jest.spyOn(notify, 'success');

      fixture = TestBed.createComponent(ProfileComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.followUser();

      expect(component.user).toBe(followedUser);
      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(updatedLoggedUser);
      expect(notifySpy).toHaveBeenCalled();
      expect(notifySpy).toHaveBeenCalledWith(`You followed ${followedUser.username}!`);
    });
  });

  describe('unfollowUser method', () => {
    it('expect usersService.unfollowUser to have been called with correct args', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const loggedUser = { id: '1' };
      const updatedLoggedUser = { id: '3' };
      const posts = [];
      const usersRelation = 2;

      activatedRoute.data = of({ data: { user, posts, usersRelation } });

      const spy = jest.spyOn(usersService, 'unfollowUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      fixture = TestBed.createComponent(ProfileComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.unfollowUser();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(user.id);
    });
  });
});
