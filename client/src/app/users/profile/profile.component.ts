import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../models/user.dto';
import { UsersService } from '../../core/services/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PostDTO } from '../../post/models/post.dto';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { UsersRelation } from '../../config/users-relation.enum';
import { PostService } from '../../post/services/post.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: UserDTO;
  loggedUser: UserDTO;
  isProfileLoggedUser: boolean;
  usersRelation: UsersRelation;
  posts: PostDTO[] = [];
  routerSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private notify: ToastrService,
    private postsService: PostService,
    private usersService: UsersService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(({ data }) => {
      this.user = data.user;
      this.posts = data.posts;
      this.usersRelation = data.usersRelation;
    }, () => { });

    this.authService.loggedUserData$.subscribe((loggedUser) => {
      if (loggedUser && +loggedUser.id === +this.user.id) { this.user = loggedUser; }
      this.usersRelation = this.authService.checkRelation(this.user, loggedUser);
      this.loggedUser = loggedUser;
    }, () => { });
  }

  followUser() {
    this.usersService.followUser(this.user.id)
      .subscribe(this.userRelationChange('followed'), () => { });
  }

  unfollowUser() {
    this.usersService.unfollowUser(this.user.id)
      .subscribe(this.userRelationChange('unfollowed'), () => { });
  }

  private userRelationChange(msg: string) {
    return ({ user, loggedUser }) => {
      this.user = user;
      this.authService.emitUserData(loggedUser);
      this.notify.success(`You ${msg} ${this.user.username}!`);
    };
  }
}
