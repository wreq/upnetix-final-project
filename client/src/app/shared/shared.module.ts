import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatTabsModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommentListComponent } from './comment-list/comment-list.component';
import { CommentComponent } from './comment/comment.component';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { RouterModule } from '@angular/router';
import { PostComponent } from './post/post.component';
import { DragDropDirective } from '../directives/drag-drop/drag-drop.directive';
import { PostListComponent } from './post-list/post-list.component';
import { PostModalComponent } from './post-modal/post-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SmallUserProfileComponent } from './small-user-profile/small-user-profile.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { SpinnerComponent } from './spinner/spinner.component';
import { PostStatusFiterPipe } from '../users/pipes/post-status-filter.pipe';
import { DateFromNowPipe } from '../core/pipes/date-from-now-pipe';

@NgModule({
  declarations: [
    CommentListComponent,
    CommentComponent,
    CommentFormComponent,
    PostComponent,
    DragDropDirective,
    PostListComponent,
    PostModalComponent,
    SmallUserProfileComponent,
    SpinnerComponent,
    PostStatusFiterPipe,
    DateFromNowPipe,
  ],
  imports: [
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-right',
    }),
    CommonModule,
    FontAwesomeModule,
    MatTabsModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    NgbModule,
    InfiniteScrollModule,
  ],
  exports: [
    CommonModule,
    FontAwesomeModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    CommentListComponent,
    CommentComponent,
    CommentFormComponent,
    PostComponent,
    PostListComponent,
    NgbModule,
    DragDropDirective,
    SmallUserProfileComponent,
    ClickOutsideModule,
    InfiniteScrollModule,
    SpinnerComponent,
    PostStatusFiterPipe,
    DateFromNowPipe
  ],
  entryComponents: [PostModalComponent]
})
export class SharedModule { }
