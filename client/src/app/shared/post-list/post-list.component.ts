import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostDTO } from '../../post/models/post.dto';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostModalComponent } from '../post-modal/post-modal.component';
import { PostStatus } from '../../config/post-status.enum';
import { PostService } from '../../post/services/post.service';
import { CONFIG } from '../../config/config';
import { combineLatest } from 'rxjs';
import { timer } from 'rxjs';
import { UsersRelation } from '../../config/users-relation.enum';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input() filter: PostStatus;
  @Input() userId: string;
  @Input() usersRelation: UsersRelation;
  @Output() postsChange = new EventEmitter();

  postsValue: PostDTO[] = [];
  loading = false;

  @Input()
  get posts() {
    return this.postsValue;
  }

  set posts(posts: PostDTO[]) {
    this.postsValue = posts;
    this.postsChange.emit(posts);
  }

  take = CONFIG.PROFILE_PAGE_POSTS_TAKE;
  skip = 0;

  constructor(
    private modalService: NgbModal,
    private postService: PostService,
  ) { }

  get UsersRelation() { return UsersRelation; }

  ngOnInit() {
    if (+this.filter === 0) {
      this.skip = this.postsValue.length;
    } else {
      this.skip = this.postsValue.reduce((acc, el) => el.postStatus === this.filter ? acc += 1 : acc, 0);
    }

    if (this.posts.filter((x) => x.postStatus === PostStatus.Private).length < this.take) {
      this.onScroll();
    }
  }

  open(post: PostDTO) {
    const modalRef = this.modalService.open(PostModalComponent, { windowClass: 'post-modal' });
    modalRef.componentInstance.post = post;
    modalRef.result
      .then((deletedPost) => this.posts = this.posts.filter((x) => x.id !== deletedPost.id))
      .catch(() => { });
    modalRef.componentInstance.updatePost.subscribe((updatedPost) => {
      this.posts = this.postsValue.map((x) => x.id === updatedPost.id ? updatedPost : x);
    });
  }

  onScroll() {
    this.loading = true;
    combineLatest(timer(1000), this.postService.getUserPosts(this.userId, this.skip, this.take, this.filter))
      .subscribe(([, data]) => {
        if (data.length) {
          this.posts = this.posts.concat(data);
          this.skip += this.take;
        }
        this.loading = false;
      }, () => this.loading = false);
  }
}
