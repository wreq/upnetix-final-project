import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked } from '@angular/core';
import { CommentDTO } from '../../core/models/comment.dto';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent {

  @Input() comments: CommentDTO[];
  @Input() isLiked: boolean;

  @Output() liked = new EventEmitter();
  @Output() editedReview = new EventEmitter();
  @Output() createComment = new EventEmitter();

  constructor() { }

  handleLike(reviewId: number) {
    this.liked.emit(reviewId);
  }

  handleEditedReview(comment: CommentDTO) {
    this.editedReview.emit(comment);
  }
}
