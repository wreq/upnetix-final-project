import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OnDestroy, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { PostService } from '../../post/services/post.service';
import { PostDTO } from '../../post/models/post.dto';
import { CommentDTO } from '../../core/models/comment.dto';
import { CommentsService } from '../../core/services/comments.service';
import { UsersRelation } from '../../config/users-relation.enum';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../users/models/user.dto';
import * as moment from 'moment';
import { PostStatus } from '../../config/post-status.enum';
import { Subscription } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'app-post-modal',
  templateUrl: './post-modal.component.html',
  styleUrls: ['./post-modal.component.css']
})
export class PostModalComponent implements OnInit, AfterViewChecked, OnDestroy {

  @ViewChild('scrollMe', { static: false }) private commentsScrollContainer: ElementRef;

  @Input() post: PostDTO;
  usersRelation: UsersRelation;
  isPostLiked = false;
  isPostPrivate: boolean;
  loggedUser: UserDTO;
  comments: CommentDTO[] = [];
  deleteConfirm = false;
  deleteEmitter = new EventEmitter();
  updatePost = new EventEmitter();
  socketSub: Subscription;
  toScroll = false;
  firebaseSub: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    private postsService: PostService,
    private commentsService: CommentsService,
    private notify: ToastrService,
    private authService: AuthService,
    private firebaseDB: AngularFireDatabase,
  ) { }

  ngOnInit() {
    this.postsService.getPostComments(this.post.id).subscribe((data) => {
      this.comments = data || [];
      this.toScroll = true;
    }, () => { });

    this.authService.loggedUserData$.subscribe((loggedUser) => {
      if (loggedUser && loggedUser.liked) {
        this.isPostLiked = loggedUser.liked.some((x) => x.id === this.post.id);
      }
      this.usersRelation = this.authService.checkRelation(this.post.user, loggedUser);
      this.loggedUser = loggedUser;
    }, () => { });

    this.firebaseSub = this.firebaseDB.object(`comment`).valueChanges()
      .pipe(skip(1))
      .subscribe((comment: any) => {
        if ((comment && comment.user && this.loggedUser && (+(comment as any).user.id === +this.loggedUser.id)) ||
          comment.postId !== this.post.id) {
          return;
        }

        this.comments.push((comment as any));
        this.toScroll = true;
      }, () => { });
  }

  ngAfterViewChecked() {
    if (this.toScroll) {
      this.scrollToBottom();
    }
    this.toScroll = false;
  }

  get UsersRelation() { return UsersRelation; }

  get PostStatus() { return PostStatus; }

  createComment(comment: CommentDTO) {
    this.commentsService.createComment(this.post.id, comment).subscribe((data) => {
      this.comments.push(data);
      this.firebaseDB.object(`comment`).update({ ...data, postId: this.post.id });
      this.toScroll = true;
    }, () => { });
  }

  likePost() {
    this.postsService.likePost(this.post.id).subscribe(({ user, post }) => {
      if (this.isPostLiked) {
        this.notify.success('Post disliked');
      } else {
        this.notify.success('Post liked');
      }
      this.post = post;
      this.authService.emitUserData(user);
    }, () => { });
  }

  deletePost() {
    this.postsService.deletePost(this.post.id).subscribe((data) => {
      this.notify.warning('Post Deleted!');
      this.activeModal.close(this.post);
    }, () => { });
  }

  toggleDeleteConfirm() {
    this.deleteConfirm = !this.deleteConfirm;
  }

  changePostStatus(status: PostStatus) {
    this.postsService.updatePost(this.post.id, { postStatus: status })
      .subscribe((post) => {
        this.post = post;
        this.updatePost.emit(post);
        if (post.postStatus === PostStatus.Public) {
          this.notify.success('Post is now public');
        } else {
          this.notify.success('Post is now private');
        }
      }, () => {});
  }

  scrollToBottom() {
    try {
      this.commentsScrollContainer.nativeElement.scrollTop = this.commentsScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  ngOnDestroy() {
    this.firebaseSub.unsubscribe();
  }
}
