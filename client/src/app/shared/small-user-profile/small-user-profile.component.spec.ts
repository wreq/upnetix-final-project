import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { UsersService } from '../../core/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { CommentListComponent } from '../comment-list/comment-list.component';
import { CommentComponent } from '../comment/comment.component';
import { CommentFormComponent } from '../comment-form/comment-form.component';
import { PostComponent } from '../post/post.component';
import { DragDropDirective } from '../../directives/drag-drop/drag-drop.directive';
import { PostListComponent } from '../post-list/post-list.component';
import { PostModalComponent } from '../post-modal/post-modal.component';
import { SmallUserProfileComponent } from './small-user-profile.component';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatTabsModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PostStatusFiterPipe } from '../../users/pipes/post-status-filter.pipe';
import { SpinnerComponent } from '../spinner/spinner.component';

describe('Small user profile component', () => {

  const authService = {
    get loggedUserData$() {
      return of();
    },
    emitUserData(user) {

    },
    checkRelation(user, loggedUser) {
      return 42;
    }
  };

  const usersService = {
    followUser() {
      return of();
    },
    unfollowUser() {
      return of();
    }
  };

  const notify = {
    success(msg) {

    }
  };

  let router: Router;
  let fixture: ComponentFixture<SmallUserProfileComponent>;
  let component: SmallUserProfileComponent;

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ToastrModule.forRoot({
          timeOut: 2000,
          positionClass: 'toast-bottom-right',
        }),
        CommonModule,
        FontAwesomeModule,
        MatTabsModule,
        ReactiveFormsModule,
        RouterModule,
        FormsModule,
        NgbModule,
        InfiniteScrollModule,
      ],
      declarations: [
        CommentListComponent,
        CommentComponent,
        CommentFormComponent,
        PostComponent,
        DragDropDirective,
        PostListComponent,
        PostModalComponent,
        SmallUserProfileComponent,
        SpinnerComponent,
        PostStatusFiterPipe,
      ],
      providers: [
        UsersService,
      ]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(ToastrService, { useValue: notify })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  it('should be defined', () => {
    fixture = TestBed.createComponent(SmallUserProfileComponent);
    component = fixture.componentInstance;

    expect(component).toBeDefined();
  });

  it('expect target user to be logged user if they have the same id', () => {

    const user = { id: '1' };
    const loggedUser = { id: '1', following: [] };

    // create the component and run all changes on init
    fixture = TestBed.createComponent(SmallUserProfileComponent);
    component = fixture.componentInstance;
    component.user = user as any;
    fixture.detectChanges();

    const spy = jest
      .spyOn(authService, 'checkRelation')
      .mockReturnValue(42);

    jest
      .spyOn(authService, 'loggedUserData$', 'get')
      .mockReturnValue(of(loggedUser));

    component.ngOnInit();

    expect(component.user).toBe(loggedUser);
    expect(component.usersRelation).toEqual(42);
    expect(spy).toHaveBeenCalledWith(loggedUser, loggedUser);
  });

  describe('followUser method', () => {
    it('expect usersService.followUser to have been called with correct args', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const updatedLoggedUser = { id: '3' };

      const spy = jest.spyOn(usersService, 'followUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      fixture = TestBed.createComponent(SmallUserProfileComponent);
      component = fixture.componentInstance;
      component.user = user as any;
      fixture.detectChanges();

      component.followUser();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(user.id);
    });

    it('expect userRelationChange to return correct function', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const updatedLoggedUser = { id: '3' };

      jest.spyOn(usersService, 'followUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      const spy = jest.spyOn(authService, 'emitUserData');

      const notifySpy = jest.spyOn(notify, 'success');

      fixture = TestBed.createComponent(SmallUserProfileComponent);
      component = fixture.componentInstance;
      component.user = user as any;
      fixture.detectChanges();

      component.followUser();

      expect(component.user).toBe(followedUser);
      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(updatedLoggedUser);
      expect(notifySpy).toHaveBeenCalled();
      expect(notifySpy).toHaveBeenCalledWith(`You followed ${followedUser.username}!`);
    });
  });

  describe('unfollowUser method', () => {
    it('expect usersService.unfollowUser to have been called with correct args', () => {

      const user = { id: '1' };
      const followedUser = { id: '2', username: 'gosho' };
      const updatedLoggedUser = { id: '3' };

      const spy = jest.spyOn(usersService, 'unfollowUser')
        .mockImplementation(() => of({ user: followedUser, loggedUser: updatedLoggedUser }));

      fixture = TestBed.createComponent(SmallUserProfileComponent);
      component = fixture.componentInstance;
      component.user = user as any;
      fixture.detectChanges();

      component.unfollowUser();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith(user.id);
    });
  });
});
