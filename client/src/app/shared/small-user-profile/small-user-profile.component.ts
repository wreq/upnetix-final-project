import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsersRelation } from '../../config/users-relation.enum';
import { ToastrService } from 'ngx-toastr';
import { UserDTO } from '../../users/models/user.dto';
import { UsersService } from '../../core/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-small-user-profile',
  templateUrl: './small-user-profile.component.html',
  styleUrls: ['./small-user-profile.component.css'],
})
export class SmallUserProfileComponent implements OnInit {

  @Input() user: UserDTO;
  usersRelation: UsersRelation;

  @Output() unfollow = new EventEmitter<UserDTO>();

  constructor(
    private authService: AuthService,
    private usersService: UsersService,
    private notify: ToastrService,
  ) { }

  ngOnInit() {
    this.authService.loggedUserData$.subscribe((loggedUser) => {
      if (loggedUser && +loggedUser.id === +this.user.id) { this.user = loggedUser; }
      this.usersRelation = this.authService.checkRelation(this.user, loggedUser);
    }, () => { });
  }

  get UsersRelation() { return UsersRelation; }

  followUser() {
    this.usersService.followUser(this.user.id)
      .subscribe(this.userRelationChange('followed'), () => { });
  }

  unfollowUser() {
    this.usersService.unfollowUser(this.user.id).pipe(
      tap((data) => {
        if (data) { return this.unfollow.emit(data.user); }
      })
    )
    .subscribe(this.userRelationChange('unfollowed'), () => { });
  }

  private userRelationChange(msg: string) {
    return ({ user, loggedUser }) => {
      this.user = user;
      this.authService.emitUserData(loggedUser);
      this.notify.success(`You ${msg} ${this.user.username}!`);
    };
  }
}
