import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PostDTO } from '../../post/models/post.dto';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() post: PostDTO;
  @Input() isUserAdmin: boolean;

  @Output() postCoverClick = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {
  }

  postClickHandle() {
    this.postCoverClick.emit(this.post);
  }
}
