import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  commentForm: FormGroup;

  @Input() commentContent: string;
  @Output() commentReady = new EventEmitter();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      content: ['', [Validators.required]],
    });
  }

  onSubmit(event) {
    event.preventDefault();
    this.commentReady.emit(this.commentForm.value);
    this.commentForm.reset();
  }
}
