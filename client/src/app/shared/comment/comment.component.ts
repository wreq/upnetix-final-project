import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CommentDTO } from '../../core/models/comment.dto';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../users/models/user.dto';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  canEdit = false;
  editForm: FormGroup;
  isUserAdmin: boolean;
  isUserAuthor: boolean;
  loggedUser: UserDTO;

  @Input() comment: CommentDTO;
  isLiked: boolean;

  @Output() liked = new EventEmitter();


  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      content: ['', [Validators.required]],
    });
  }


}
