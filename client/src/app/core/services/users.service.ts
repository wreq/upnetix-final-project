import { Injectable } from '@angular/core';
import { UserDTO } from '../../users/models/user.dto';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  baseUrl = CONFIG.API_DOMAIN_NAME;

  public constructor(private readonly http: HttpClient) {}

  getAllUsers(query?: string): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(`${this.baseUrl}/users${query ? `?q=${query}` : ''}`);
  }

  deleteUser(userId: string) {
    return this.http.delete(`${this.baseUrl}/users/${userId}`);
  }

  updateBanStatus(userId: string, banStatus: any) {
    return this.http.put(`${this.baseUrl}/users/${userId}/banstatus`, banStatus);
  }

  getUser(userId: string): Observable<UserDTO> {
    return this.http.get<UserDTO>(`${this.baseUrl}/users/${userId}`);
  }

  updateLoggedUser(user: UserDTO): Observable<{ user: UserDTO, token: string }> {
    return this.http.put<{ user: UserDTO, token: string }>(`${this.baseUrl}/users`, user);
  }

  followUser(userId: string) {
    return this.http.patch<{ user: UserDTO, loggedUser: UserDTO }>(`${this.baseUrl}/users/${userId}`, { action: 'followUser' });
  }

  unfollowUser(userId: string) {
    return this.http.patch<{ user: UserDTO, loggedUser: UserDTO }>(`${this.baseUrl}/users/${userId}`, { action: 'unfollowUser' });
  }
}
