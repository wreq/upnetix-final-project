import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { StorageService } from '../storage.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private readonly storageService: StorageService,
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ) {
    const token = this.storageService.getItem('token') || '';
    let updatedRequest;

    if (request.url.includes('imgur')) {
      updatedRequest = request.clone({
        headers: request.headers.set('Authorization', `Client-ID ea389cc6f050f6e`)
      });
    } else {
      updatedRequest = request.clone({
        headers: request.headers.set('Authorization', `Bearer ${token}`)
      });
    }

    return next.handle(updatedRequest);
  }
}
