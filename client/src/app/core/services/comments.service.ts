import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CommentDTO } from '../models/comment.dto';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  baseUrl = CONFIG.API_DOMAIN_NAME;

  public constructor(private readonly http: HttpClient) {}

  createComment(postId: string, comment: CommentDTO): Observable<CommentDTO> {
    return this.http.post<CommentDTO>(`${this.baseUrl}/posts/${postId}/comment`, comment);
  }
}
