import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDTO } from '../../users/models/user.dto';
import { UserCredentialsDTO } from '../../users/models/user-credentials.dto';
import { CONFIG } from '../../config/config';
import { UsersRelation } from '../../config/users-relation.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(this.getUserDataIfAuthenticated());
  loggedUserData$ = this.loggedUserDataSubject$.asObservable();

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService
  ) { }

  public emitUserData(user: UserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): UserDTO {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');

      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  public register(user: UserCredentialsDTO): Observable<UserCredentialsDTO> {
    return this.http.post<UserCredentialsDTO>(
      `${CONFIG.API_DOMAIN_NAME}/users`,
      user
    );
  }

  public login(userCredentials): Observable<any> {
    return this.http
      .post<{ user: UserDTO, token: string }>(`${CONFIG.API_DOMAIN_NAME}/users/login`, userCredentials)
      .pipe(
        tap(({ user, token }) => {
          this.storage.setItem('token', token);
          this.emitUserData(user);
        })
      );
  }

  public logout(): void {
    this.storage.clear();
    this.loggedUserDataSubject$.next(null);
  }

  checkRelation(user: UserDTO, loggedUser: UserDTO): UsersRelation {
    if (!loggedUser || !loggedUser.following) {
      return 0;
    }

    if (+user.id === +loggedUser.id) {
      return UsersRelation.Me;
    } else {
      return loggedUser.following.some((x) => x.id === user.id) ?
        UsersRelation.Following : UsersRelation.NotFollowing;
    }
  }
}
