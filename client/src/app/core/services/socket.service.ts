import { Injectable } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { AuthService } from './auth.service';
import { CommentDTO } from '../models/comment.dto';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socketSubject$: WebSocketSubject<any>;
  private subscription: Subscription;

  private readonly commentDataSubject$ = new BehaviorSubject<CommentDTO>(null);

  constructor(
    private readonly authService: AuthService,
  ) {
    this.authService.loggedUserData$.subscribe(
      (user) => {
        !!user ? this.start() : this.stop();
      }
      , () => { });
  }

  get commentData$() {
    return this.commentDataSubject$.asObservable();
  }

  private start() {
    this.socketSubject$ = new WebSocketSubject(`ws://localhost:8081/`);

    this.subscription = this.socketSubject$.
      subscribe((data) => {
        this.commentDataSubject$.next(data);
      });

    // authenticate
    this.socketSubject$.next({ token: localStorage.getItem('token') || '' });
  }

  private stop() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    if (this.socketSubject$) {
      this.socketSubject$.complete();
    }
  }
}
