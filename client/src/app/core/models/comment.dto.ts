import { UserDTO } from '../../users/models/user.dto';

export class CommentDTO {
  id: string;
  createdAt: string;
  user: UserDTO;
  content: string;
  flags: number;
  likes: number;
  dislikes: number;
  vote: { isLiked: boolean, isDisliked: boolean };
  flag: { isFlagged: boolean };
}
