import { PostStatus } from '../../config/post-status.enum';
import { UserDTO } from '../../users/models/user.dto';

export class CreatePostDTO {
  id: string;
  coverUrl: string;
  postStatus?: PostStatus;
  user: UserDTO;
}
