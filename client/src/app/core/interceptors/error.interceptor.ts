import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  public constructor(
    private readonly router: Router,
    private readonly notify: ToastrService
  ) {}

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.status === 404) {
            this.notify.error('Page not found');
            this.router.navigate(['not-found']);
          } else if (event.status === 500) {
            this.notify.error('Server error');
            this.router.navigate(['server-error']);
          } else if (event.status === 401) {
            this.notify.error('Unauthorised');
            this.router.navigate(['/users/login']);
          }
          return event;
        }
      })
    );
  }
}
