import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateFromNowPipe' })
export class DateFromNowPipe implements PipeTransform {
  transform(date) {
    const currentDate = new Date();
    const lastTwoWeeks = moment(currentDate).subtract(14, 'days');

    if (moment(date).isBefore(lastTwoWeeks)) {
      return moment(date).format('MMM Do YY');
    }

    return  moment(date).fromNow();
  }
}
