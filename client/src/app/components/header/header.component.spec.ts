import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes, Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuard } from '../../core/guards/auth.guard';
import { SharedModule } from '../../shared/shared.module';
import { CoreModule } from '../../core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from '../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { NotFoundComponent } from '../not-found/not-found.component';
import { ServerErrorComponent } from '../server-error/server-error.component';
import { UsersService } from '../../core/services/users.service';
import { HeaderComponent } from './header.component';
import { SearchDropdownComponent } from '../search-dropdown/search-dropdown.component';
import { AppComponent } from '../../app.component';
import { FooterComponent } from '../footer/footer.component';

describe('HeaderComponent', () => {

  const routes: Routes = [
    { path: '', redirectTo: 'posts', pathMatch: 'full' },
    {
      path: 'users',
      loadChildren: () => import('../../users/users.module').then(m => m.UsersModule)
    },
    {
      path: 'posts',
      loadChildren: () => import('../../post/posts.module').then(m => m.PostsModule)
    },
    { path: 'not-found', component: NotFoundComponent },
    { path: 'server-error', component: ServerErrorComponent },
    { path: '**', redirectTo: '/not-found' }
  ];

  const authService = {
    get loggedUserData$() {
      return of();
    },
    logout() { },
  };

  const usersService = {
    getAllUsers(query) {
      return of();
    }
  };

  const notify = {
    success(msg) {

    }
  };

  let router: Router;
  let fixture: ComponentFixture<HeaderComponent>;
  let component: HeaderComponent;

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        NotFoundComponent,
        ServerErrorComponent,
        SearchDropdownComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        BrowserAnimationsModule,
        CoreModule,
        BrowserModule,
        HttpClientModule,
        NoopAnimationsModule,
        JwtModule.forRoot({ config: {} }),
      ]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(UsersService, { useValue: usersService })
      .overrideProvider(ToastrService, { useValue: notify })
      .compileComponents();

    router = TestBed.get(Router);
  }));

  it('should be defined', () => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeDefined();
  });


  it('expect to get data from loggedUserData subject', () => {

    const loggedUser = { id: '1' };

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const spy = jest.spyOn(authService, 'loggedUserData$', 'get')
      .mockImplementation(() => of(loggedUser));

    component.ngOnInit();

    expect(component.loggedUserData).toBe(loggedUser);
  });

  it('expect expect on NavigationStart event search.search() and properties changed', () => {

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const spy = jest.spyOn(component.search, 'reset');

    component.ngOnInit();
    TestBed.get(Router).events.next(new NavigationStart(1, 'fake', 'fake' as any));


    expect(spy).toHaveBeenCalled();
    expect(component.isSearchFocused).toEqual(false);
    expect(component.isCollapsed).toEqual(true);
  });

  describe('logout method', () => {
    it('expect authService logout to be called once', () => {

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      const spy = jest.spyOn(authService, 'logout');

      component.logout();

      expect(spy).toHaveBeenCalled();
    });

    it('expect notify success to be called once with correct args', () => {

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      const spy = jest.spyOn(notify, 'success');

      component.logout();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith('Successful logout!');
    });

    it('expect rotuer navigate to be called once with correct args', () => {

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      jest.spyOn(router, 'navigate');

      component.logout();

      expect(router.navigate).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/users/login']);
    });
  });

  describe('searchInputHandle method', () => {
    it('expect usersService getAllUsers to be called once with correct args if no users.length is 0', () => {

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.search.value.query = 'test';

      const spy = jest.spyOn(usersService, 'getAllUsers');
      jest.spyOn(authService, 'loggedUserData$', 'get');

      component.searchInputHandle();

      expect(spy).toHaveBeenCalled();
      expect(spy).toHaveBeenCalledWith('test');
    });

    it('expect usersService getAllUsers return data to be consumed if no users.length is 0', () => {
      const testUsers = [];
      const loggedUser = { id: '42' };

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      component.search.value.query = 'test';
      fixture.detectChanges();

      const spy = jest.spyOn(usersService, 'getAllUsers')
        .mockImplementation(() => of(testUsers));
      jest.spyOn(authService, 'loggedUserData$', 'get')
        .mockImplementation(() => of(loggedUser));

      component.searchInputHandle();

      expect(component.unfilteredUsers).toBe(testUsers);
    });

    it('expect logged user ot be removed from users if no users.length is 0', () => {
      const testUsers = [{ id: '42' }];
      const loggedUser = { id: '42' };

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.search.value.query = 'test';

      const spy = jest.spyOn(usersService, 'getAllUsers')
        .mockImplementation(() => of(testUsers));
      jest.spyOn(authService, 'loggedUserData$', 'get')
        .mockImplementation(() => of(loggedUser));

      component.searchInputHandle();

      expect(component.users).not.toContain(loggedUser);
    });

    it('expect users to be filtered by name if query is \'someting\'', () => {
      const user = { username: 'ab' };

      fixture = TestBed.createComponent(HeaderComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.unfilteredUsers = [user, { username: 'cd'}];
      component.users = [user, { username: 'cd'}];
      component.search.value.query = 'a';

      component.searchInputHandle();

      expect(component.users.length).toEqual(1);
      expect(component.users).toContain(user);
    });
  });
});


