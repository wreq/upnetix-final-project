import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription, of } from 'rxjs';
import { Router, NavigationStart } from '@angular/router';
import { UsersService } from '../../core/services/users.service';
import { switchMap, tap, filter, catchError } from 'rxjs/operators';
import { UserDTO } from '../../users/models/user.dto';
import { AuthService } from '../../core/services/auth.service';
import { StorageService } from '../../core/services/storage.service';
import { Roles } from '../../config/role.enum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {


  isCollapsed = true;
  isAdmin = false;
  loggedUserData: UserDTO;
  isSearchFocused = false;
  users = [];
  unfilteredUsers = [];
  search: FormGroup;

  private loggedUserSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private usersService: UsersService,
    private notify: ToastrService,
    private router: Router,
  ) {
    this.search = this.formBuilder.group({
      query: [''],
    });
  }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$
    .subscribe((loggedUser) => this.loggedUserData = loggedUser, () => {});

    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    ).subscribe((event: NavigationStart) => {
      this.search.reset();
      this.isSearchFocused = false;
      this.isCollapsed = true;
    }, () => {});
  }

  logout() {
    this.authService.logout();
    this.notify.success('Successful logout!');
    this.router.navigate(['/users/login']);
  }

  searchInputHandle() {
    if (!this.users.length) {
      this.usersService.getAllUsers(this.search.value.query).pipe(
        tap((data) => (this.users = data, this.unfilteredUsers = data)),
        switchMap(() => this.authService.loggedUserData$),
        tap((loggedUser) => this.users = this.users.filter((x) => x.id !== loggedUser.id)),
        catchError(() => of([]))
      ).subscribe();
    } else if (this.search.value.query === '') {
      this.users = [];
      this.unfilteredUsers = [];
    } else {
      this.users = this.unfilteredUsers.filter((x) => x.username.includes(this.search.value.query));
    }
  }

  handleClickOutsideSearch(ev) {
    if (ev.target.className.includes('form-control')) {
      return;
    }
    this.isSearchFocused = false;
  }

  handleClickOutsideHeader(ev) {
    this.isCollapsed = true;
  }

  ngOnDestroy() {
    this.loggedUserSubscription.unsubscribe();
  }
}
