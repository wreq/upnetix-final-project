import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserDTO } from '../../users/models/user.dto';

@Component({
  selector: 'app-search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.css']
})
export class SearchDropdownComponent implements OnInit {

  @Input() users: UserDTO[];

  constructor() { }

  ngOnInit() {
  }

}
