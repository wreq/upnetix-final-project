import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserDTO } from '../users/models/user.dto';
import { UsersService } from '../core/services/users.service';
import { AuthService } from '../core/services/auth.service';
import { tap, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserResolverService implements Resolve<UserDTO> {

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<UserDTO> {
    const payload = this.authService.getUserDataIfAuthenticated();
    if (payload) {
      return this.usersService.getUser(payload.id)
      .pipe(
        tap((loggedUser) => this.authService.emitUserData(loggedUser))
      );
    }
  }
}

