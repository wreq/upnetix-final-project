export enum UsersRelation {
  Me = 1,
  Following = 2,
  NotFollowing = 3,
}
