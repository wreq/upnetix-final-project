import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None // Needed for adding css to body, app-root tag.
})
export class AppComponent implements OnInit {
  title = 'telerik-library-client';

  constructor(
    private router: Router,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    ).subscribe((event: NavigationStart) => {
      this.modalService.dismissAll();
    }, () => {});
  }
}
