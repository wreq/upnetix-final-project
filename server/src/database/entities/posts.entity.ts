import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, ManyToMany, CreateDateColumn, RelationCount } from 'typeorm';
import { User } from './users.entity';
import { PostStatus } from '../../common/postStatus';
import { Comment } from './comments.entity';

@Entity('posts')
export class Post extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'nvarchar', length: 100, nullable: false })
  caption: string;

  @Column({ type: 'nvarchar', nullable: false})
  coverUrl: string;

  @Column({ type: 'enum', enum: PostStatus, default: PostStatus.Public})
  postStatus: PostStatus;

  @CreateDateColumn()
  createdAt: string;

  @Column({type: 'boolean', default: false})
  isDeleted: boolean;

  @ManyToOne(type => User, user => user.posts, { eager: true })
  user: User;

  @ManyToMany(type => User, user => user.liked)
  likedBy: Promise<User[]>;

  @RelationCount((post: Post) => post.likedBy)
  likesCount: number;

  @OneToMany(type => Comment, comments => comments.post)
  comments: Promise<Comment[]>;

  @Column({type: 'numeric', default: 0})
  commentsCount: number;
}
