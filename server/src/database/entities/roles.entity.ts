import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';
import { RoleType } from '../../common/role';

@Entity('roles')
export class Role {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'enum', enum: RoleType, default: RoleType.Basic })
  role: RoleType;
}
