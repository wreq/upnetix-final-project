import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, JoinColumn, ManyToMany, CreateDateColumn } from 'typeorm';
import { User } from './users.entity';
import { Post } from './posts.entity';

@Entity('comments')
export class Comment extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'nvarchar', length: 100, nullable: false })
  content: string;

  @Column({ type: 'bool', default: false })
  isDeleted: boolean;

  @CreateDateColumn()
  createdAt: string;

  @ManyToOne(type => User, user => user.comments, { eager: true })
  user: User;

  @ManyToOne(type => Post, post => post.comments)
  post: Promise<Post>;
}
