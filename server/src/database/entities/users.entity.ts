import { BaseEntity, PrimaryGeneratedColumn, Column, Entity, ManyToMany, JoinTable, OneToMany, RelationCount} from 'typeorm';
import { Role } from './roles.entity';
import { Post } from './posts.entity';
import { Comment } from './comments.entity';

@Entity('users')
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'nvarchar', unique: true, length: 20})
  username: string;

  @Column({ type: 'nvarchar', unique: true, length: 20})
  email: string;

  @Column('nvarchar')
  password: string;

  @Column({ type: 'nvarchar', default: '', length: 200})
  publicInfo: string;

  @Column({type: 'nvarchar', default: 'https://i.imgur.com/u62Rlx8.jpg'})
  profilePicUrl: string;

  @Column('nvarchar')
  salt: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @ManyToMany(type => Role, { eager: true })
  @JoinTable()
  roles: Role[];

  @ManyToMany(type => User, user => user.following)
  @JoinTable()
  followers: Promise<User[]>;

  @ManyToMany(type => User, user => user.followers)
  following: Promise<User[]>;

  @RelationCount((user: User) => user.followers)
  followersCount: number;

  @RelationCount((user: User) => user.following)
  followingCount: number;

  @Column({ type: 'boolean', default: false })
  isBanned: boolean;

  @Column({ type: 'nvarchar', default: 'no desc'})
  description: string;

  @OneToMany(type => Post, posts => posts.user)
  posts: Promise<Post[]>;

  @RelationCount((user: User) => user.posts)
  postsCount: number;

  @ManyToMany(type => Post, post => post.likedBy)
  @JoinTable()
  liked: Promise<Post[]>;

  @OneToMany(type => Comment, comments => comments.user)
  comments: Promise<Comment[]>;
}
