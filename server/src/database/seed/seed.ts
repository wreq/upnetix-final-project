import { createConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from '../entities/roles.entity';
import { User } from '../entities/users.entity';
import { RoleType } from '../../common/role';
import { Post } from '../entities/posts.entity';
import { HttpService } from '@nestjs/common';
import { tap, map, switchMap } from 'rxjs/operators';

const main = async () => {

  const httpService = new HttpService();

  const connection = await createConnection();
  const roleRepo = connection.getRepository(Role);
  const userRepo = connection.getRepository(User);
  const postRepo = connection.getRepository(Post);

  const admin = await roleRepo.save({
    role: RoleType.Admin,
  });

  const basic = await roleRepo.save({
    role: RoleType.Basic,
  });

  const salt = await bcrypt.genSalt();
  let firstAdmin = new User();
  firstAdmin.username = 'John',
    firstAdmin.email = 'john@asdf.com';
  firstAdmin.password = await bcrypt.hash('testing', salt);
  firstAdmin.salt = salt;
  firstAdmin.roles = [basic, admin];
  firstAdmin = await userRepo.save(firstAdmin);

  let firstPost = new Post();
  firstPost.caption = 'ADMIN POST';
  firstPost.coverUrl = 'https://i.annihil.us/u/prod/marvel/i/mg/6/40/5db8423029ef1/clean.jpg';
  firstPost.user = firstAdmin;
  firstPost = await postRepo.save(firstPost);

  // const createPost = async (cover) => {
  //   if (!cover) { return; }
  //   const post = new Post();
  //   post.caption = 'ADMIN POST';
  //   post.coverUrl = cover;
  //   post.user = firstAdmin;
  //   return await postRepo.save(post);
  // };

  // httpService.get(`https://api.imgur.com/3/gallery/random/random`, {
  //   headers: {
  //     Authorization: `Client-ID ea389cc6f050f6e`,
  //   },
  // }).pipe(
  //   map((data) => data.data.data.map((x) => x.link)),
  //   switchMap((covers) => Promise.all(covers.map(createPost))),
  //   switchMap(() => connection.close()),
  // ).subscribe();

  connection.close();

  // tslint:disable: no-console
  console.log(`Data seeded successfully`);

};

main()
  .catch(console.log);
