import { createConnection } from 'typeorm';
import { User } from '../entities/users.entity';
import { Post } from '../entities/posts.entity';
import { HttpService } from '@nestjs/common';
import { map, switchMap } from 'rxjs/operators';

const main = async () => {

  const httpService = new HttpService();

  const connection = await createConnection();
  const userRepo = connection.getRepository(User);
  const postRepo = connection.getRepository(Post);

  const foundUser = await userRepo.findOne({ where: { id: '1'}});

  const createPost = async (cover) => {
    if (!cover) { return; }
    const post = new Post();
    post.caption = 'ADMIN POST';
    post.coverUrl = cover;
    post.user = foundUser;
    return await postRepo.save(post);
  };

  httpService.get(`https://api.imgur.com/3/gallery/random/random`, {
    headers: {
      Authorization: `Client-ID ea389cc6f050f6e`,
    },
  }).pipe(
    map((data) => data.data.data.map((x) => x.link)),
    switchMap((covers) => Promise.all(covers.map(createPost))),
    switchMap(() => connection.close()),
  ).subscribe();

  // tslint:disable: no-console
  console.log(`Data seeded successfully`);

};

main()
  .catch(console.log);
