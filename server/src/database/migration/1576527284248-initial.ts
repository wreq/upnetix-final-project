import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1576527284248 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `roles` (`id` int NOT NULL AUTO_INCREMENT, `role` enum ('1', '2') NOT NULL DEFAULT '1', PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `posts` (`id` int NOT NULL AUTO_INCREMENT, `caption` varchar(100) NOT NULL, `coverUrl` varchar(255) NOT NULL, `postStatus` enum ('1', '2') NOT NULL DEFAULT '1', `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `commentsCount` decimal NOT NULL DEFAULT 0, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(20) NOT NULL, `email` varchar(20) NOT NULL, `password` varchar(255) NOT NULL, `publicInfo` varchar(200) NOT NULL DEFAULT '', `profilePicUrl` varchar(255) NOT NULL DEFAULT 'https://i.imgur.com/u62Rlx8.jpg', `salt` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `isBanned` tinyint NOT NULL DEFAULT 0, `description` varchar(255) NOT NULL DEFAULT 'no desc', UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `comments` (`id` int NOT NULL AUTO_INCREMENT, `content` varchar(100) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` int NULL, `postId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users_roles_roles` (`usersId` int NOT NULL, `rolesId` int NOT NULL, INDEX `IDX_df951a64f09865171d2d7a502b` (`usersId`), INDEX `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`), PRIMARY KEY (`usersId`, `rolesId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users_followers_users` (`usersId_1` int NOT NULL, `usersId_2` int NOT NULL, INDEX `IDX_8d63f6043394b4d32343bdea11` (`usersId_1`), INDEX `IDX_1433e3275a501bc09f5c33c7ca` (`usersId_2`), PRIMARY KEY (`usersId_1`, `usersId_2`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `users_liked_posts` (`usersId` int NOT NULL, `postsId` int NOT NULL, INDEX `IDX_95bdaeb12a846d72a312cbf63e` (`usersId`), INDEX `IDX_ecf726248159919a504b7e0ba7` (`postsId`), PRIMARY KEY (`usersId`, `postsId`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `posts` ADD CONSTRAINT `FK_ae05faaa55c866130abef6e1fee` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_7e8d7c49f218ebb14314fdb3749` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_e44ddaaa6d058cb4092f83ad61f` FOREIGN KEY (`postId`) REFERENCES `posts`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_followers_users` ADD CONSTRAINT `FK_8d63f6043394b4d32343bdea11d` FOREIGN KEY (`usersId_1`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_followers_users` ADD CONSTRAINT `FK_1433e3275a501bc09f5c33c7ca2` FOREIGN KEY (`usersId_2`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_liked_posts` ADD CONSTRAINT `FK_95bdaeb12a846d72a312cbf63e4` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `users_liked_posts` ADD CONSTRAINT `FK_ecf726248159919a504b7e0ba79` FOREIGN KEY (`postsId`) REFERENCES `posts`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_liked_posts` DROP FOREIGN KEY `FK_ecf726248159919a504b7e0ba79`", undefined);
        await queryRunner.query("ALTER TABLE `users_liked_posts` DROP FOREIGN KEY `FK_95bdaeb12a846d72a312cbf63e4`", undefined);
        await queryRunner.query("ALTER TABLE `users_followers_users` DROP FOREIGN KEY `FK_1433e3275a501bc09f5c33c7ca2`", undefined);
        await queryRunner.query("ALTER TABLE `users_followers_users` DROP FOREIGN KEY `FK_8d63f6043394b4d32343bdea11d`", undefined);
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_b2f0366aa9349789527e0c36d97`", undefined);
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_df951a64f09865171d2d7a502b1`", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_e44ddaaa6d058cb4092f83ad61f`", undefined);
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_7e8d7c49f218ebb14314fdb3749`", undefined);
        await queryRunner.query("ALTER TABLE `posts` DROP FOREIGN KEY `FK_ae05faaa55c866130abef6e1fee`", undefined);
        await queryRunner.query("DROP INDEX `IDX_ecf726248159919a504b7e0ba7` ON `users_liked_posts`", undefined);
        await queryRunner.query("DROP INDEX `IDX_95bdaeb12a846d72a312cbf63e` ON `users_liked_posts`", undefined);
        await queryRunner.query("DROP TABLE `users_liked_posts`", undefined);
        await queryRunner.query("DROP INDEX `IDX_1433e3275a501bc09f5c33c7ca` ON `users_followers_users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_8d63f6043394b4d32343bdea11` ON `users_followers_users`", undefined);
        await queryRunner.query("DROP TABLE `users_followers_users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_b2f0366aa9349789527e0c36d9` ON `users_roles_roles`", undefined);
        await queryRunner.query("DROP INDEX `IDX_df951a64f09865171d2d7a502b` ON `users_roles_roles`", undefined);
        await queryRunner.query("DROP TABLE `users_roles_roles`", undefined);
        await queryRunner.query("DROP TABLE `comments`", undefined);
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`", undefined);
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`", undefined);
        await queryRunner.query("DROP TABLE `users`", undefined);
        await queryRunner.query("DROP TABLE `posts`", undefined);
        await queryRunner.query("DROP TABLE `roles`", undefined);
    }

}
