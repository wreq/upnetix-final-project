import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';
import { PostStatus } from '../../common/postStatus';
import { ShowUserDTO } from '../users/showUser.dto';

export class ShowPostDTO {
    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish()
    public caption: string;

    @ApiModelProperty()
    @Publish()
    public coverUrl: string;

    @ApiModelProperty()
    @Publish()
    public postStatus: PostStatus;

    @ApiModelProperty()
    @Publish(ShowUserDTO)
    public user: ShowUserDTO;

    @ApiModelProperty()
    @Publish()
    public commentsCount: number;

    @ApiModelProperty()
    @Publish()
    public likesCount: number;

    @Publish()
    public createdAt: string;
}
