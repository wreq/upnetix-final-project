import { IsNotEmpty, IsString, MinLength, MaxLength, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { PostStatus } from '../../common/postStatus';

export class CreatePostDTO {

    @ApiModelProperty({
        minLength: 0,
        maxLength: 100,
        example: 'Nice pic!',
    })
    @IsString()
    @IsOptional()
    @MinLength(0)
    @MaxLength(100)
    caption: string;

    @IsString()
    @MinLength(0)
    @MaxLength(666)

    coverUrl: string;

    @IsString()
    @IsNotEmpty()
    postStatus: PostStatus;

}
