import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator';
import { PostStatus } from '../../common/postStatus';

export class UpdatePostDTO {

    @IsOptional()
    @IsString()
    @MinLength(0)
    @MaxLength(100)
    caption: string;

    @IsOptional()
    postStatus: PostStatus;
}
