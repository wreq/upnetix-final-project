import { IsNotEmpty, IsString, MinLength, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateCommentDTO {
  @ApiModelProperty({
    minLength: 0,
    maxLength: 800,
  })
  @IsNotEmpty()
  @IsString()
  @MinLength(0)
  @MaxLength(800)
  content: string;
}
