import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';
import { PostStatus } from '../../common/postStatus';
import { ShowUserDTO } from '../users/showUser.dto';
import { User } from '../../database/entities/users.entity';

export class ShowCommentDTO {
    @ApiModelProperty()
    @Publish()
    public id: string;

    @ApiModelProperty()
    @Publish(ShowUserDTO)
    public user: User;

    @ApiModelProperty()
    @Publish()
    public content: string;
}
