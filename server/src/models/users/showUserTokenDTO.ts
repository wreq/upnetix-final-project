import { ApiModelProperty } from '@nestjs/swagger';
import { Role } from '../../database/entities/roles.entity';
import { Publish } from '../../transformer/decorators/publish';
import { ShowUserDTO } from './showUser.dto';

export class ShowUserTokenDTO {
  @ApiModelProperty()
  @Publish(ShowUserDTO)
  user: ShowUserDTO;

  @ApiModelProperty()
  @Publish()
  token: string;
}
