import { IsString, MinLength, MaxLength, IsNotEmpty, IsBoolean } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class UpdateBanStatusDTO {
  // Documentation
  @ApiModelProperty()
  // Validation
  @IsBoolean()
  @Type(() => Boolean)
  isBanned: boolean;

  // Documentation
  @ApiModelProperty()
  // Validation
  @IsNotEmpty()
  @IsString()
  @MinLength(5)
  @MaxLength(40)
  description: string;
}
