import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';

export class ShowFollowingDTO {

  // Documentation
  @ApiModelProperty()
  @Publish()
  id: string;

  @ApiModelProperty({
    minLength: 4,
    maxLength: 20,
    example: 'Petko',
  })
  @Publish()
  username: string;

  @Publish()
  profilePicUrl: string;
}
