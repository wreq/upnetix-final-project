import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';
import { ShowUserDTO } from './showUser.dto';
import { User } from '../../database/entities/users.entity';

export class ShowWithLoggedUserDTO {
  @ApiModelProperty()
  @Publish(ShowUserDTO)
  user: ShowUserDTO;

  @ApiModelProperty()
  @Publish(ShowUserDTO)
  loggedUser: User;
}
