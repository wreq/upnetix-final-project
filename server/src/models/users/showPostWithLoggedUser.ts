import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';
import { ShowUserDTO } from './showUser.dto';
import { ShowPostDTO } from '../posts/showPost.dto';
import { Post } from '../../database/entities/posts.entity';
import { User } from '../../database/entities/users.entity';

export class ShowPostWithLoggedUser {
  @ApiModelProperty()
  @Publish(ShowPostDTO)
  post: Post;

  @ApiModelProperty()
  @Publish(ShowUserDTO)
  user: User;
}
