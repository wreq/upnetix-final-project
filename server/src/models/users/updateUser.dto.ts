import { IsString, MinLength, MaxLength, IsOptional, IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserDTO {
  @ApiModelProperty()
  @IsString()
  @MinLength(2)
  @MaxLength(50)
  @IsOptional()
  username: string;

  @ApiModelProperty()
  @IsString()
  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @MinLength(6)
  @MaxLength(20)
  @IsOptional()
  @IsNotEmpty()
  password: string;

  @IsString()
  @MinLength(0)
  @MaxLength(200)
  @IsOptional()
  publicInfo: string;

  @IsString()
  @MinLength(5)
  @IsOptional()
  profilePicUrl: string;
}
