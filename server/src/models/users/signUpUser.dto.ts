import { IsString, MinLength, MaxLength, IsNotEmpty, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class SignUpUserDTO {
  // Documentation
  @ApiModelProperty({
    minLength: 4,
    maxLength: 20,
    example: 'Petko',
  })
  // Validation
  @IsNotEmpty()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  // Documentation
  @ApiModelProperty({
    minLength: 4,
    maxLength: 20,
    example: 'Petko',
  })
  // Validation
  @IsNotEmpty()
  @IsString()
  // @IsEmail()
  @MinLength(3)
  @MaxLength(20)
  email: string;

  // Documentation
  @ApiModelProperty({
    minLength: 6,
    maxLength: 20,
    example: '1234aB',
  })
  // Validation
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  password: string;
}
