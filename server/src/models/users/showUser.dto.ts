import { ApiModelProperty } from '@nestjs/swagger';
import { Role } from '../../database/entities/roles.entity';
import { Publish } from '../../transformer/decorators/publish';
import { ShowRoleDTO } from './role.dto';
import { User } from '../../database/entities/users.entity';
import { ShowFollowingDTO } from './showFollowing.dto';
import { Post } from '../../database/entities/posts.entity';
import { ShowPostDTO } from '../posts/showPost.dto';

export class ShowUserDTO {

  // Documentation
  @ApiModelProperty()
  @Publish()
  id: string;

  @ApiModelProperty({
    minLength: 4,
    maxLength: 20,
    example: 'Petko',
  })
  @Publish()
  username: string;

  @Publish()
  email: string;

  @Publish()
  publicInfo: string;

  @Publish()
  profilePicUrl: string;

  @Publish(ShowFollowingDTO)
  followers: Promise<User[]>;

  @Publish(ShowFollowingDTO)
  following: Promise<User[]>;

  @Publish()
  followersCount: number;

  @Publish()
  followingCount: number;

  @Publish(ShowPostDTO)
  liked: Promise<Post[]>;

  @Publish()
  postsCount: number;

  @ApiModelProperty()
  @Publish(ShowRoleDTO)
  roles: Role[];

  @ApiModelProperty()
  @Publish()
  isBanned: boolean;

  @ApiModelProperty()
  @Publish()
  description: string;

  @ApiModelProperty()
  @Publish()
  isDeleted: boolean;
}
