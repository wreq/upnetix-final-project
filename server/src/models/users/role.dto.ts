import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';

export class ShowRoleDTO {
  @ApiModelProperty()
  id: string;

  @Publish()
  role: number;
}
