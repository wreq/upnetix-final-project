import { Controller, UseInterceptors, SetMetadata, Get, Post, Body, ValidationPipe, Param, UseGuards, Delete } from '@nestjs/common';
import { ApiUseTags, ApiOkResponse, ApiBearerAuth } from '@nestjs/swagger';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { ShowPostDTO } from '../models/posts/showPost.dto';
import { RoleType } from '../common/role';
import { CommentsService } from './comments.service';
import { ShowCommentDTO } from '../models/comments/showComment.dto';
import { CreateCommentDTO } from '../models/comments/createComment.dto';
import { GetUser } from '../decorators/getUser.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../users/guards/role.guard';

@ApiUseTags('comments')
@Controller('')
export class CommentsController {

  constructor(
    private readonly commentsService: CommentsService,
  ) { }

  @ApiOkResponse({ description: 'Commnet successfully created' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowCommentDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Basic])
  @Post('posts/:postId/comment')
  public async createComment(
    @GetUser('id') userId: string,
    @Param('postId') postId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true }),
    ) comment: CreateCommentDTO): Promise<ShowCommentDTO> {
    return await this.commentsService.createComment(userId, postId, comment);
  }

  @ApiOkResponse({ description: 'Commet successfully deleted' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowCommentDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Basic])
  @Delete('posts/:postId/comments/:commentId')
  public async deleteComment(
    @Param('commentId') commentId: string,
  ): Promise<ShowCommentDTO> {
    return await this.commentsService.deleteComment(commentId);
  }

  @ApiOkResponse({ description: 'Comments successfully received' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowCommentDTO))
  @SetMetadata('roles', [RoleType.Basic])
  @Get('posts/:postId/comments')
  public async getPostComments(
    @Param('postId') postId: string,
  ): Promise<ShowCommentDTO[]> {
    return await this.commentsService.getPostComments(postId);
  }
}
