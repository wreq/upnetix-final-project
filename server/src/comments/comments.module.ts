import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/users.entity';
import { Comment } from '../database/entities/comments.entity';
import { Post } from '../database/entities/posts.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Post, Comment]),
  ],
  controllers: [CommentsController],
  providers: [CommentsService],
})
export class CommentsModule {}
