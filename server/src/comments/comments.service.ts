import { Injectable } from '@nestjs/common';
import { Comment } from '../database/entities/comments.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCommentDTO } from '../models/comments/createComment.dto';
import { Post } from '../database/entities/posts.entity';
import { User } from '../database/entities/users.entity';
import { SocialMediaAppError } from '../exceptions/social-media-app-error';

@Injectable()
export class CommentsService {

  constructor(
    @InjectRepository(Comment) private commentsRepository: Repository<Comment>,
    @InjectRepository(Post) private postsRepository: Repository<Post>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    ) {}

  async createComment(userId: string, postId: string, comment: CreateCommentDTO): Promise<Comment> {
    const newComment = this.commentsRepository.create(comment);
    const foundUser = await this.usersRepository.findOne({ where: { id: userId }});
    if (!foundUser) {
      throw new SocialMediaAppError('User not found');
    }

    const foundPost = await this.postsRepository.findOne({ where: { id: postId }});
    if (!foundPost) {
      throw new SocialMediaAppError('Post not found');
    }

    newComment.user = foundUser;
    newComment.post = Promise.resolve(foundPost);

    const savedComment = await this.commentsRepository.save(newComment);

    [, foundPost.commentsCount] = await this.commentsRepository.findAndCount({ where: { post: foundPost.id,  isDeleted: false }});
    await this.postsRepository.save(foundPost);

    return savedComment;
  }

  async deleteComment(commentId: string): Promise<Comment> {
    const foundComment = await this.commentsRepository.findOne({ where: { id: commentId }});

    if (!foundComment) {
      throw new SocialMediaAppError('Comment not found');
    }

    foundComment.isDeleted = true;

    return await this.commentsRepository.save(foundComment);
  }

  async getPostComments(postId: string): Promise<Comment[]> {
    return await this.commentsRepository.find({ where: { post: postId, isDeleted: false }, order: { createdAt: 'ASC' }});
  }
}
