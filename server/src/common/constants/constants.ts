
export const Constants = {
  bcryptType: Symbol.for('bcrypt'),
  jwtExpireTime: 3600,
};
