import * as bcrypt from 'bcrypt';
import { Constants } from './constants/constants';

export const bcryptProvider = {
  provide: Constants.bcryptType,
  useValue: bcrypt,
};
