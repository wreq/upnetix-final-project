import { Role } from '../database/entities/roles.entity';

export interface IPayload {
  id: string;
  username: string;
  roles: Role[];
  isBanned: boolean;
}
