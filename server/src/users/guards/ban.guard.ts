import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { User } from '../../database/entities/users.entity';

@Injectable()
export class BanGuard implements CanActivate {
  constructor() {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user: User = request.user;
    return user && !user.isBanned;
  }
}
