import { UsersController } from './users.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { UsersService } from './users.service';
import { SignInUserDTO } from '../models/users/signInUser.dto';
import { UpdateBanStatusDTO } from '../models/users/updateBanStatus.dto';

describe('UsersController', () => {
  let controller: UsersController;

  let usersService: any;

  beforeEach(async () => {
    usersService = {
      signUp() {
        /* empty */
      },
      signIn() {
        /* empty */
      },
      updateBanStatus() {
        /* empty */
      },
      deleteUser() {
        /* empty */
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: usersService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(controller).toBeDefined();
  });

  describe('registerUser()', () => {
    it('should call usersService signUp() once', async () => {
      // Arrange
      const mockUser = { username: 'John' };

      const spy = jest
        .spyOn(usersService, 'signUp');

      // Act
      await controller.registerUser((mockUser as SignInUserDTO));

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser);
    });

    it('should return usersService signUp() result', async () => {
      // Arrange
      const mockUser = { username: 'John' };
      const returnValue = 'test';

      jest
        .spyOn(usersService, 'signUp')
        .mockReturnValue(Promise.resolve(returnValue));

      // Act
      const result = await controller.registerUser((mockUser as SignInUserDTO));

      // Assert
      expect(result).toBe(returnValue);
    });
  });

  describe('loginUser()', () => {
    it('should call usersService signIn() once', async () => {
      // Arrange
      const mockUser = { username: 'John' };

      const spy = jest
        .spyOn(usersService, 'signIn');

      // Act
      await controller.loginUser((mockUser as SignInUserDTO));

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mockUser);
    });

    it('should return usersService signIn() result', async () => {
      // Arrange
      const mockUser = { username: 'John' };
      const returnValue = 'test';

      jest
        .spyOn(usersService, 'signIn')
        .mockReturnValue(Promise.resolve(returnValue));

      // Act
      const result = await controller.loginUser((mockUser as SignInUserDTO));

      // Assert
      expect(result).toBe(returnValue);
    });
  });

  describe('updateBanStatus()', () => {
    it('should call usersService updateBanStatus() once', async () => {
      // Arrange
      const userId: string = 'test';
      const mockBanStatus = { isBanned: true };

      const spy = jest
        .spyOn(usersService, 'updateBanStatus');

      // Act
      await controller.updateBanStatus(userId, (mockBanStatus as UpdateBanStatusDTO));

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(userId, mockBanStatus);
    });

    it('should return usersService updateBanStatus() result', async () => {
      // Arrange
      const userId: string = 'test';
      const mockBanStatus = { isBanned: true };
      const returnValue = 'returnTest';

      jest
        .spyOn(usersService, 'updateBanStatus')
        .mockReturnValue(Promise.resolve(returnValue));

      // Act
      const result = await controller.updateBanStatus(userId, (mockBanStatus as UpdateBanStatusDTO));

      // Assert
      expect(result).toBe(returnValue);
    });
  });

  describe('deleteUser()', () => {
    it('should call usersService deleteUser() once', async () => {
      // Arrange
      const userId: string = 'test';

      const spy = jest
        .spyOn(usersService, 'deleteUser');

      // Act
      await controller.deleteUser(userId);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(userId);
    });

    it('should return usersService deleteUser() result', async () => {
      // Arrange
      const userId: string = 'test';
      const returnValue = 'returnTest';

      jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(Promise.resolve(returnValue));

      // Act
      const result = await controller.deleteUser(userId);

      // Assert
      expect(result).toBe(returnValue);
    });
  });
});
