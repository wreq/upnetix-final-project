// tslint:disable-next-line: max-line-length
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/users.entity';
import { Repository, MoreThan, Like, In, Any } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Role } from '../database/entities/roles.entity';
import { RoleType } from '../common/role';
import { SignInUserDTO } from '../models/users/signInUser.dto';
import { SignUpUserDTO } from '../models/users/signUpUser.dto';
import { UpdateBanStatusDTO } from '../models/users/updateBanStatus.dto';
import { IPayload } from '../common/payload';
import { ShowUserTokenDTO } from '../models/users/showUserTokenDTO';
import { SocialMediaAppError } from '../exceptions/social-media-app-error';
import { UpdateUserDTO } from '../models/users/updateUser.dto';
import { PostStatus } from '../common/postStatus';
import { Post } from '../database/entities/posts.entity';
import { CommentsController } from '../comments/comments.controller';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
    @InjectRepository(Post) private readonly postRepository: Repository<Post>,
    private readonly jwtService: JwtService,
  ) { }

  public async signUp(user: SignUpUserDTO): Promise<User> {

    const { username, email, password } = user;

    if (await this.userRepository.findOne({ username })) {
      throw new SocialMediaAppError('Username taken');
    }

    if (await this.userRepository.findOne({ email })) {
      throw new SocialMediaAppError('Email taken');
    }

    const salt = await bcrypt.genSalt();
    const newUser = this.userRepository.create(user);
    const basicRole = await this.roleRepository.findOne({ role: RoleType.Basic });

    if (!basicRole) {
      throw new SocialMediaAppError('No user role found. Seed before first register');
    }

    newUser.username = username;
    newUser.roles = [basicRole];
    newUser.password = await bcrypt.hash(password, salt);
    newUser.salt = salt;

    return await this.userRepository.save(newUser);
  }

  public async signIn(user: SignInUserDTO): Promise<ShowUserTokenDTO> {
    const { usernameOrEmail, password } = user;

    let foundUser: User;

    foundUser = await this.userRepository.findOne({ where: { email: usernameOrEmail }, relations: ['following', 'followers', 'liked'] });

    if (!foundUser) {
      foundUser = await this.validateUser({ username: usernameOrEmail });
    }

    const userHash = await bcrypt.hash(password, foundUser.salt);
    if (userHash !== foundUser.password) {
      throw new SocialMediaAppError('Ivalid password');
    }

    const payload: IPayload = {
      id: foundUser.id,
      username: foundUser.username,
      roles: foundUser.roles,
      isBanned: foundUser.isBanned,
    };

    const token = this.jwtService.sign(payload);

    return { user: foundUser, token };
  }

  public async getAllUsers(usernameQuery?: string) {
    return await this.userRepository.find(
      {
        where: { username: usernameQuery ? Like(`%${usernameQuery}%`) : Like(`%%`), isDeleted: false },
        relations: ['followers', 'following', 'liked']
      },
    );
  }

  public async getUser(userId: string) {
    const foundUser = await this.userRepository.findOne({ where: { id: userId }, relations: ['followers', 'following', 'liked'] });

    if (!foundUser) {
      throw new SocialMediaAppError('User not found');
    }

    return foundUser;
  }

  public async updateBanStatus(userId: string, banstatus: UpdateBanStatusDTO): Promise<User> {
    const { isBanned, description } = banstatus;
    const foundUser = await this.validateUser({ id: userId });

    foundUser.isBanned = isBanned;
    foundUser.description = description;

    return await this.userRepository.save(foundUser);
  }

  public async updateUser(userId: string, user: UpdateUserDTO): Promise<ShowUserTokenDTO> {
    const foundUser = await this.validateUser({ id: userId });

    Object.keys(user).forEach((prop: string) => {
      if ((user as any)[prop] !== undefined) {
        (foundUser as any)[prop] = (user as any)[prop];
      }
    });

    if (user.password) {
      const salt = await bcrypt.genSalt();
      foundUser.password = await bcrypt.hash(user.password, salt);
    }

    const payload: IPayload = {
      id: foundUser.id,
      username: foundUser.username,
      roles: foundUser.roles,
      isBanned: foundUser.isBanned,
    };
    const updatedUser = await this.userRepository.save(foundUser);
    const token = this.jwtService.sign(payload);

    return { user: updatedUser, token };
  }

  public async deleteUser(userId: string): Promise<User> {
    const foundUser = await this.validateUser({ id: userId });

    foundUser.isDeleted = true;

    return await this.userRepository.save(foundUser);
  }

  public async followUser(userId: string, userToFollowId: string): Promise<{ user: User, loggedUser: User }> {
    if (userId === userToFollowId) {
      throw new SocialMediaAppError('You cannot follow yourself');
    }

    const foundUser = await this.validateUser({ id: userId });
    const foundUserToFollow = await this.validateUser({ id: userToFollowId });

    const followers = await foundUserToFollow.followers;
    if (followers.some(x => x.id === userId)) {
      throw new SocialMediaAppError('You already follow this user');
    }

    followers.push(foundUser);
    await this.userRepository.save(foundUserToFollow);

    const user = await this.userRepository.findOne({ where: { id: foundUserToFollow.id }, relations: ['followers', 'following', 'liked'] });
    const loggedUser = await this.userRepository.findOne({ where: { id: userId }, relations: ['followers', 'following', 'liked'] });

    return { user, loggedUser };
  }

  public async unfollowUser(userId: string, userToFollowId: string): Promise<{ user: User, loggedUser: User }> {
    if (userId === userToFollowId) {
      throw new SocialMediaAppError('You cannot follow yourself');
    }

    const foundUser = await this.validateUser({ id: userId });
    const foundUserToUnfollow = await this.validateUser({ id: userToFollowId });

    const followers = await foundUserToUnfollow.followers;
    if (!followers.some(x => x.id === userId)) {
      throw new SocialMediaAppError('You are not following this user');
    }

    followers.splice(followers.indexOf(foundUser), 1);

    await this.userRepository.save(foundUserToUnfollow);

    const user = await this.userRepository.findOne({ where: { id: foundUserToUnfollow.id }, relations: ['followers', 'following', 'liked'] });
    const loggedUser = await this.userRepository.findOne({ where: { id: userId }, relations: ['followers', 'following', 'liked'] });

    return { user, loggedUser };
  }

  public async validateUser(findOptions: Partial<User>): Promise<User> {
    const user = await this.userRepository.findOne(findOptions);

    if (!user) {
      throw new SocialMediaAppError('User not found');
    }

    await user.followers;
    await user.following;
    await user.liked;

    return user;
  }

  public async getUserPosts(userId: string, loggedUserId: string, skip: number, take: number, postStatus?: PostStatus): Promise<Post[]> {
    const permission = +postStatus === PostStatus.Public ? true :
      (await this.isUserFollowing(loggedUserId, userId) || +userId === +loggedUserId);

    if (!permission) {
      throw new SocialMediaAppError('You have to follow the user to see his private posts');
    }

    return await this.postRepository.createQueryBuilder('post')
      .where('post.user = :id', { id: userId })
      .andWhere('post.postStatus IN (:...status)', { status: postStatus ? [postStatus] : [1, 2] })
      .andWhere('post.isDeleted = :bool', { bool: false })
      .leftJoinAndSelect('post.user', 'user')
      .leftJoinAndSelect('user.followers', 'followers')
      .skip(skip)
      .take(take)
      .getMany();
  }

  private async isUserFollowing(loggedUserId: string, userId: string) {
    const foundLoggedUser = await this.userRepository
      .findOne({ where: { id: loggedUserId }, relations: ['following'] });
    const foundUser = await this.userRepository.findOne({ where: { id: userId } });

    if (!(foundLoggedUser && foundUser)) {
      throw new SocialMediaAppError('User not found');
    }

    return (await foundLoggedUser.following).some((x) => x.id === foundUser.id);
  }
}
