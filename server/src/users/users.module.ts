import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/users.entity';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './strategy/jwt.strategy';
import { Role } from '../database/entities/roles.entity';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { PostsService } from '../posts/posts.service';
import { Post } from '../database/entities/posts.entity';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.jwtSecret,
        signOptions: {
          expiresIn: 3600, // JWT expires after 1 hour
        },
      }),
    }),
    TypeOrmModule.forFeature([User, Post, Role])],
  providers: [
    ConfigService,
    UsersService,
    PostsService,
    JwtStrategy,
  ],
  controllers: [UsersController],
})
export class UsersModule { }
