import { UsersService } from './users.service';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../database/entities/users.entity';
import { Role } from '../database/entities/roles.entity';
import { JwtService } from '@nestjs/jwt';
import { Constants } from '../common/constants/constants';
import { ConflictException, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { RoleType } from '../common/role';

describe('UsersService', () => {
  let service: UsersService;
  let userRepo: any;
  let roleRepo: any;
  let jwtServiceMock: any;
  let bcryptMock: any;

  beforeEach(async () => {
    userRepo = {
      find() {
        /* empty */
      },
      findOne() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      },
    };

    roleRepo = {
      findOne() {
        /* empty */
      },
    };

    jwtServiceMock = {
      sign() {
        /* empty */
      },
    };

    bcryptMock = {
      genSalt() {
        /* empty */
      },
      hash() {
        /* empty */
      },
      compare() {
        /* empty */
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: getRepositoryToken(User), useValue: userRepo },
        { provide: getRepositoryToken(Role), useValue: roleRepo },
        { provide: JwtService, useValue: jwtServiceMock },
        { provide: Constants.bcryptType, useValue: bcryptMock },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('signUp()', () => {
    it('should call userRepository findOne() once with correct parameters', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      const spy = jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledWith({ username: user.username });
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should throw if userRepository findOne() returns a user', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      const spy = jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => user);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Assert && Act
      expect(service.signUp(user)).rejects.toThrow(ConflictException);
    });

    it('call bcrypt genSalt() once', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      const spy = jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('call bcrypt hash() once with correct parameters', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => 'salt');
      const spy = jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith('pass', 'salt');
    });

    it('call userRepository create() once', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      const spy = jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it('call roleRepository findOne() once with correct parameters', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      const spy = jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({ role: RoleType.Basic });
    });

    it('should throw if roleRepository findOne() returns undefined', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: 'pass',
        salt: 'salt',
      };
      const spy = jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => true);
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'test');

      // Assert && Act
      expect(service.signUp(user)).rejects.toThrow(InternalServerErrorException);
    });

    it('should call userRepository save() once with correct parameters', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: '',
        salt: '',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => 'role');
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => 'salt');
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'pass');
      const spy = jest
        .spyOn(userRepo, 'save')
        .mockImplementation(() => '');

      const outputUser = {
        username: 'name',
        roles: ['role'],
        password: 'pass',
        salt: 'salt',
      };

      // Act
      await service.signUp(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(outputUser);
    });

    it('should return the result of userRepository save()', async () => {
      // Arrange
      const user = {
        username: 'name',
        roles: [],
        password: '',
        salt: '',
      };
      jest
        .spyOn(userRepo, 'findOne')
        .mockImplementation(() => undefined);
      jest
        .spyOn(userRepo, 'create')
        .mockImplementation(() => user);
      jest
        .spyOn(roleRepo, 'findOne')
        .mockImplementation(() => 'role');
      jest
        .spyOn(bcryptMock, 'genSalt')
        .mockImplementation(() => 'salt');
      jest
        .spyOn(bcryptMock, 'hash')
        .mockImplementation(() => 'pass');
      const spy = jest
        .spyOn(userRepo, 'save')
        .mockImplementation(() => 'output');

      // Act
      const result = await service.signUp(user);

      // Assert
      expect(result).toBe('output');
    });
  });

  describe('signIn()', () => {
    it('should call bcrypt compare() with correct arguments', async () => {
      // Arrange
      const user = {
        username: '',
        roles: [],
        password: 'test',
        salt: '',
      };
      jest.spyOn(jwtServiceMock, 'sign');
      jest.spyOn(userRepo, 'findOne')
      .mockImplementation(() => ({password: 'testPass'}));
      const spy = jest
      .spyOn(bcryptMock, 'compare')
      .mockImplementation(() =>  true);

      // Act
      await service.signIn(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenLastCalledWith(user.password, 'testPass');
    });

    it('should throw if bcrypt compare() returns undefiend', async () => {
      // Arrange
      const user = {
        username: '',
        roles: [],
        password: 'test',
        salt: '',
      };
      jest.spyOn(jwtServiceMock, 'sign');
      jest.spyOn(userRepo, 'findOne')
      .mockImplementation(() => ({password: 'testPass'}));
      const spy = jest
      .spyOn(bcryptMock, 'compare')
      .mockImplementation(() =>  undefined);

      // Assert && Act
      expect(service.signIn(user)).rejects.toThrow(UnauthorizedException);
    });

    it('should call jwtService sign() with correct arguments', async () => {
      // Arrange
      const user = {
        username: '',
        roles: [],
        password: 'test',
        salt: '',
      };
      const foundUser = {
        id: 'testId',
        roles: ['testRole'],
      }
      const spy = jest.spyOn(jwtServiceMock, 'sign');
      jest.spyOn(userRepo, 'findOne')
      .mockImplementation(() => foundUser);
      jest
      .spyOn(bcryptMock, 'compare')
      .mockImplementation(() =>  true);

      // Act
      await service.signIn(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenLastCalledWith({ id: foundUser.id, roles: foundUser.roles });
    });

    it('should return correct result', async () => {
      // Arrange
      const user = {
        username: '',
        roles: [],
        password: '',
        salt: '',
      };
      const foundUser = {
        id: 'testId',
        roles: ['testRole'],
      };
      jest.spyOn(jwtServiceMock, 'sign')
      .mockImplementation(() => 'test');
      jest.spyOn(userRepo, 'findOne')
      .mockImplementation(() => foundUser);
      jest
      .spyOn(bcryptMock, 'compare')
      .mockImplementation(() =>  true);

      // Act
      const result = await service.signIn(user);

      // Assert
      expect(result).toEqual({ user: foundUser, token: 'test'});
    });
  });

  describe('updateBanStatus()', () => {
    it('should call userRepository save() with correct parameter', async () => {
      // Arrange
      const banStatus = {
        isBanned: true,
        description: 'testDesc',
      };
      jest
      .spyOn(userRepo, 'findOne')
      .mockImplementation(() => ({ name: 'user', isBanned: false, description: ''}));
      const spy = jest.spyOn(userRepo, 'save');

      // Act
      await service.updateBanStatus('1', banStatus);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({ name: 'user', isBanned: true, description: 'testDesc'});
    });

    it('should correct result', async () => {
      // Arrange
      const banStatus = {
        isBanned: true,
        description: 'testDesc',
      };
      jest
      .spyOn(userRepo, 'findOne')
      .mockImplementation(() => true);
      jest.spyOn(userRepo, 'save')
      .mockImplementation(() => 'test');

      // Act
      const result = await service.updateBanStatus('1', banStatus);

      // Assert
      expect(result).toBe('test');
    });
  });

  describe('deleteUser()', () => {
    it('should call userRepository save() with correct parameter (user with is delete true)', async () => {
      // Arrange
      jest
      .spyOn(userRepo, 'findOne')
      .mockImplementation(() => ({ isDeleted: false}));
      const spy = jest.spyOn(userRepo, 'save');

      // Act
      await service.deleteUser('1');

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({ isDeleted: true });
    });

    it('should correct result', async () => {
      // Arrange
      const banStatus = {
        isBanned: true,
        description: 'testDesc',
      };
      jest
      .spyOn(userRepo, 'findOne')
      .mockImplementation(() => ({ isDeleted: false }));
      jest.spyOn(userRepo, 'save')
      .mockImplementation(() => 'test');

      // Act
      const result = await service.updateBanStatus('1', banStatus);

      // Assert
      expect(result).toBe('test');
    });
  });
});
