import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { IPayload } from '../../common/payload';
import { UsersService } from '../users.service';
import { ConfigService } from '../../config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
    });
  }

  async validate(payload: IPayload): Promise<IPayload> {
    const foundUser = await this.userService.validateUser({ id: payload.id });

    return { id: payload.id, username: payload.username, roles: payload.roles, isBanned: foundUser.isBanned };
  }
}
