import { Controller, Post, Body, UseGuards, ValidationPipe, SetMetadata,
   Param, Delete, UseInterceptors, Put, ParseIntPipe, Get, Query, Patch } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
// tslint:disable-next-line: max-line-length
import { ApiUseTags, ApiCreatedResponse, ApiBadRequestResponse, ApiUnauthorizedResponse, ApiOkResponse, ApiBearerAuth, ApiNotFoundResponse } from '@nestjs/swagger';
import { RolesGuard } from './guards/role.guard';
import { RoleType } from '../common/role';
import { SignUpUserDTO } from '../models/users/signUpUser.dto';
import { SignInUserDTO } from '../models/users/signInUser.dto';
import { ShowUserDTO } from '../models/users/showUser.dto';
import { UpdateBanStatusDTO } from '../models/users/updateBanStatus.dto';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { ShowUserTokenDTO } from '../models/users/showUserTokenDTO';
import { GetUser } from '../decorators/getUser.decorator';
import { User } from '../database/entities/users.entity';
import { UpdateUserDTO } from '../models/users/updateUser.dto';
import { followUserAction } from '../common/followUserActions';
import { ShowPostDTO } from '../models/posts/showPost.dto';
import { PostStatus } from '../common/postStatus';
import { ShowWithLoggedUserDTO } from '../models/users/showWIthLoggedUser.dto';

@ApiUseTags('users')
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
  ) { }

  @ApiCreatedResponse({ description: 'User successfully created.' })
  @ApiBadRequestResponse({ description: 'Username already exists.' })
  @UseInterceptors(new TransformInterceptor(ShowUserDTO))
  @Post()
  public async registerUser(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: SignUpUserDTO): Promise<ShowUserDTO> {
    return await this.usersService.signUp(user);
  }

  @ApiOkResponse({ description: 'JWT' })
  @ApiUnauthorizedResponse({ description: 'Wrong credentials' })
  @UseInterceptors(new TransformInterceptor(ShowUserTokenDTO))
  @Post('/login')
  public async loginUser(
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: SignInUserDTO,
  ): Promise<ShowUserTokenDTO> {
    return await this.usersService.signIn(user);
  }

  @ApiOkResponse({ description: 'User successfully logged out.' })
  @ApiUnauthorizedResponse({ description: 'Token maybe expired' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete('/logout')
  public async logoutUser(@GetUser() test: User): Promise<{ msg: string }> {
    return { msg: 'User successfully logged out.' };
  }

  @ApiOkResponse({ description: 'User successfully banned.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowUserDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Basic])
  @Get()
  public async getAllUser(@Query('q') usernameQuery?: string): Promise<ShowUserDTO[]> {
    return await this.usersService.getAllUsers(usernameQuery);
  }

  @ApiOkResponse({ description: 'User successfully banned.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowUserDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Basic])
  @Get(':userId')
  public async getUser(@Param('userId') userId: string): Promise<ShowUserDTO> {
    return await this.usersService.getUser(userId);
  }

  @ApiOkResponse({ description: 'User successfully banned.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowUserDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Admin, RoleType.Basic])
  @Put(':userId/banstatus')
  public async updateBanStatus(
    @Param('userId', new ParseIntPipe()) userId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) banstatus: UpdateBanStatusDTO,
  ): Promise<ShowUserDTO> {

    return await this.usersService.updateBanStatus(userId, banstatus);
  }

  @ApiOkResponse({ description: 'User successfully banned.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowUserTokenDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Admin, RoleType.Basic])
  @Put()
  public async updateLoggedUserProfile(
    @GetUser('id') userId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) user: UpdateUserDTO,
  ): Promise<ShowUserTokenDTO> {

    return await this.usersService.updateUser(userId, user);
  }

  @ApiOkResponse({ description: 'User successfully deleted.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @UseInterceptors(new TransformInterceptor(ShowUserDTO))
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Admin, RoleType.Basic])
  @Delete(':userId')
  public async deleteUser(
    @Param('userId', new ParseIntPipe()) userId: string,
  ): Promise<ShowUserDTO> {

    return await this.usersService.deleteUser(userId);
  }

  @ApiOkResponse({ description: 'User successfully follwed.' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowWithLoggedUserDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Admin, RoleType.Basic])
  @Patch(':userId')
  public async followUser(
    @GetUser('id') userId: string,
    @Param('userId', new ParseIntPipe()) userToFollowId: string,
    @Body('action') action: followUserAction,
  ): Promise<ShowWithLoggedUserDTO> {
    return await this.usersService[action](userId, userToFollowId);
  }

  @ApiOkResponse({ description: 'All posts successfully retrieved' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @SetMetadata('roles', [RoleType.Basic])
  @Get('/:userId/posts')
  public async getUserPosts(
    @Param('userId') userId: string,
    @GetUser('id') loggedUserId: string,
    @Query('skip') skip: string,
    @Query('take') take: string,
    @Query('postStatus') postStatus?: PostStatus,

  ): Promise<ShowPostDTO[]> {
    return await this.usersService.getUserPosts(userId, loggedUserId, +skip, +take, postStatus);
  }
}
