
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/users.entity';
import { Post } from '../database/entities/posts.entity';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';

@Module({
    imports: [
      TypeOrmModule.forFeature([Post, User]),
    ],
    controllers: [PostsController],
    providers: [
      PostsService,
    ],
  })
  export class PostsModule {}
