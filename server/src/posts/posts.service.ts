import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Not, Like, In, MoreThan } from 'typeorm';
import { User } from '../database/entities/users.entity';
import { Post } from '../database/entities/posts.entity';
import { CreatePostDTO } from '../models/posts/createPost.dto';
import { UpdatePostDTO } from '../models/posts/updatePost.dto';
import { PostStatus } from '../common/postStatus';
import { SocialMediaAppError } from '../exceptions/social-media-app-error';

@Injectable()
export class PostsService {

  constructor(
    @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) { }

  public async followersPosts(userId: string, skip: number, take: number): Promise<Post[]> {

    const foundUser = await this.usersRepository.findOne({ where: { id: userId, relations: ['followers', 'following', 'liked'] } });
    if (!foundUser) {
      throw new SocialMediaAppError('No such user found!');
    }
    const userFollowings = (await foundUser.following).map((x) => x.id);

    if (!userFollowings.length) {
      return [];
    }

    const foundPost = await this.postsRepository.find({
      where: { isDeleted: false, user: In(userFollowings) },
      order: { createdAt: 'DESC' },
      skip,
      take,
    });
    if (!foundPost) {
      throw new SocialMediaAppError('Post unavailable!');
    }

    return foundPost;
  }

  public async allPublicPosts(skip: number, take: number): Promise<Post[]> {
    if (!take) {
      throw new SocialMediaAppError('Take query param cannot be 0');
    }

    return await this.postsRepository.find({
      where: { postStatus: PostStatus.Public, isDeleted: false },
      order: { createdAt: 'DESC' },
      skip,
      take,
    });
  }

  public async individualPost(postId: string, userId: string): Promise<Post> {
    const foundUser: User = await this.usersRepository.findOne(userId);
    if (!foundUser) {
      throw new SocialMediaAppError('User unavailable');
    }

    const foundPost = await this.postsRepository.findOne(postId);
    if (!foundPost) {
      throw new SocialMediaAppError('Post unavailable!');
    }

    return foundPost;
  }

  public async createPost(userId: string, post: CreatePostDTO): Promise<Post> {
    const postEntity: Post = this.postsRepository.create(post);

    const foundUser: User = await this.usersRepository.findOne({ id: userId });
    if (!foundUser) {
      throw new SocialMediaAppError('User not found');
    }

    postEntity.user = foundUser;

    return this.postsRepository.save(postEntity);
  }

  public async updatePost(loggedUserId: string, postId: string, post: UpdatePostDTO): Promise<Post> {
    const foundPost: Post = await this.postsRepository.findOne({ where: { id: postId } });

    if (!foundPost) {
      throw new SocialMediaAppError('Post not found!');
    }

    if (+loggedUserId !== +foundPost.user.id) {
      throw new SocialMediaAppError('This is not your post. You cannot update it');
    }

    const updatedPost = { ...foundPost, ...post };

    return this.postsRepository.save(updatedPost);
  }

  public async likePost(userId: string, postId: string): Promise<{ user: User, post: Post }> {
    const foundUser: User = await this.usersRepository.findOne({ where: { id: userId }, relations: ['following', 'followers', 'liked'] });

    if (!foundUser) {
      throw new BadRequestException('User not found!');
    }

    const foundPost: Post = await this.postsRepository.findOne({ where: { id: postId } });

    if (!foundPost) {
      throw new BadRequestException('Post not found!');
    }

    const likedPosts = await foundUser.liked;
    const post = likedPosts.find((x) => x.id === foundPost.id);
    if (post) {
     (await foundUser.liked).splice(likedPosts.indexOf(post), 1);
    } else {
      (await foundUser.liked).push(foundPost);
    }

    const loggedUser = await this.usersRepository.save(foundUser);
    const likedPost = await this.postsRepository.findOne({ where: { id: postId } });

    return { user: loggedUser, post: likedPost };
  }

  public async deletePost(loggedUserId, postId: string): Promise<Post> {
    const foundPost: Post = await this.postsRepository.findOne(postId);
    if (!foundPost) {
      throw new SocialMediaAppError('Post not found');
    }

    if (+loggedUserId !== +foundPost.user.id) {
      throw new SocialMediaAppError('This is not your post. You cannot delete it');
    }

    foundPost.isDeleted = true;

    return this.postsRepository.save(foundPost);
  }
}
