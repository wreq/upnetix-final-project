import { PostsService } from './posts.service';
import { ApiUseTags, ApiOkResponse, ApiBearerAuth } from '@nestjs/swagger';
import {
  Controller, Get, Post, Body, Param, ValidationPipe, UseInterceptors,
  UseGuards, SetMetadata, Patch, Delete, Put, ParseIntPipe, Req, Query,
} from '@nestjs/common';
import { CreatePostDTO } from '../models/posts/createPost.dto';
import { ShowPostDTO } from '../models/posts/showPost.dto';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { RoleType } from '../common/role';
import { GetUser } from '../decorators/getUser.decorator';
import { UpdatePostDTO } from '../models/posts/updatePost.dto';
import { AuthGuard } from '@nestjs/passport';
import { likePostActions } from '../common/likePostActions';
import { ShowPostWithLoggedUser } from '../models/users/showPostWithLoggedUser';
import { RolesGuard } from '../users/guards/role.guard';

@ApiUseTags()
@Controller()
export class PostsController {
  public constructor(
    private readonly postsService: PostsService,

  ) { }

  @ApiOkResponse({ description: 'Followers posts successfully retrieved' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @SetMetadata('roles', [RoleType.Basic])
  @Get('/posts/feed')
  public async followersPosts(
    @GetUser('id') userId: string,
    @Query('skip') skip: string,
    @Query('take') take: string,
    ): Promise<ShowPostDTO[]> {
    return await this.postsService.followersPosts(userId, +skip, +take);
  }

  @ApiOkResponse({ description: 'All Public posts successfully retrieved' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @SetMetadata('roles', [RoleType.Basic])
  @Get('/posts/public')
  public async allPublicPosts(
    @Query('skip') skip: string,
    @Query('take') take: string,
  ): Promise<ShowPostDTO[]> {
    return await this.postsService.allPublicPosts(+skip, +take);
  }

  @ApiOkResponse({ description: 'Individual post successfully retrieved' })
  @ApiBearerAuth()
  @UseGuards(RolesGuard)
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @UseGuards(AuthGuard('jwt'))
  @SetMetadata('roles', [RoleType.Basic])
  @Get('/posts/:id')
  public async individualPost(@Param('id') postId: string, @GetUser('id') userId: string): Promise<ShowPostDTO> {
    return await this.postsService.individualPost(postId, userId);
  }

  @ApiOkResponse({ description: 'Post created successfully' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @SetMetadata('roles', [RoleType.Basic])
  @Post('/posts')
  public async createPost(
    @GetUser('id') userId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) post: CreatePostDTO,
    ): Promise<ShowPostDTO> {
    return await this.postsService.createPost(userId, post);
  }

  @ApiOkResponse({ description: 'Post was updated successfully' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @UseInterceptors(new TransformInterceptor(ShowPostDTO))
  @SetMetadata('roles', [RoleType.Basic])
  @Put('posts/:id')
  public async updatePost(
    @GetUser('id') loggedUserId: string,
    @Param('id', new ParseIntPipe()) postId: string,
    @Body(new ValidationPipe({ transform: true, whitelist: true })) body: UpdatePostDTO,
  ): Promise<ShowPostDTO> {
    return await this.postsService.updatePost(loggedUserId, postId, body);
  }

  @ApiOkResponse({ description: 'Post deleted succesfully' })
  @ApiBearerAuth()
  @SetMetadata('roles', [RoleType.Admin])
  @UseGuards(AuthGuard('jwt'))
  @Delete('posts/:postId')
  public async deletePost(
    @GetUser('id') loggedUserId: string,
    @Param('postId', new ParseIntPipe()) postId: string): Promise<{ msg: string }> {
    await this.postsService.deletePost(loggedUserId, postId);
    return { msg: 'Post was deleted!' };
  }

  @ApiOkResponse({ description: 'Post deleted succesfully' })
  @ApiBearerAuth()
  @UseInterceptors(new TransformInterceptor(ShowPostWithLoggedUser))
  @UseGuards(AuthGuard('jwt'))
  @SetMetadata('roles', [RoleType.Admin])
  @Patch('posts/:postId')
  public async likePost(
    @Param('postId', new ParseIntPipe()) postId: string,
    @GetUser('id') userId: string,
    @Body('action', new ValidationPipe({ transform: true, whitelist: true })) action: likePostActions,
    ): Promise<ShowPostWithLoggedUser> {
    return await this.postsService[action](userId, postId);
  }

}
