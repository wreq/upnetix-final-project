export class SocialMediaAppError extends Error {
  constructor(message: string) {
    super(message);
  }
}
