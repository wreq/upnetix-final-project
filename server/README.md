# Upnetix Social Media Application

## Description

One man’s constant is another man’s variable.

## Setup

1. Create **.env** file.   

        DB_TYPE = mysql
        DB_HOST = localhost
        DB_PORT = 3306
        DB_USERNAME = root
        DB_PASSWORD = 123
        DB_DATABASE_NAME = example
        JWT_SECRET = secret_example

2. Install dependencies.

        $ npm install

3. Run migrations.

        $ npm run typeorm -- migration:run

4. Create **ormconfig.json**.
```json
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "123",
  "database": "example",
  "synchronize": false,
  "logging": false,
  "entities": [
    "src/database/entities/**/*.ts"
  ],
  "migrations": [
    "src/database/migration/**/*.ts"
  ],
  "cli": {
    "entitiesDir": "src/database/entities",
    "migrationsDir": "src/database/migration"
  }
}
```
4. Run seed script.

        $ npm run seed

## Running the app

    $ npm run start:dev

## Documentation 

### Swagger

  http://localhost:3000/api/

  
